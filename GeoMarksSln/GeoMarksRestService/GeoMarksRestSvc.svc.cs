﻿using System;
using System.Collections.Generic;
using System.Linq;
using SmartDocuments.GeoMarksDataContracts;
using SmartDocuments.GeoMarksDataContracts.DTOs;
using SmartDocuments.GeoMarksDataContracts.Json;
using SmartDocuments.GeoMarksModel.Repository;
using System.ServiceModel.Web;
using System.Net;
using Npgsql;
using System.Globalization;

namespace SmartDocuments.GeoMarksRestService
{
    public class GeoMarksRestSvc : IGeoMarksRestSvc
    {
        public UserJson ValidateUser(string phoneNumber, string password)
        {
            var repo = new NHibernateRepository<User>();
            var roleRepo = new NHibernateRepository<UserRole>();
            User user = null;
            // хотят зайти под информатором
            if (string.IsNullOrEmpty(password))
            {
                user = repo.GetAll().FirstOrDefault(_ => _.PhoneNumber == phoneNumber);
                // если такого юзера еще не было, то создаём нового информатора с пустым паролем
                if (user == null)
                {
                    user = new User();
                    user.Email = "";
                    user.FirstName = "";
                    user.LastName = "";
                    user.Password = "";
                    user.Patronymic = "";
                    user.PhoneNumber = phoneNumber;
                    user.Role = roleRepo.Get((int) RoleEnum.Informant);
                    repo.Save(user);
                }
                else user.Role = roleRepo.Get((int) RoleEnum.Informant);
            }
            else // все, кто вводит пароль, будут заходить под своими ролями
            {
                user = repo.GetAll().FirstOrDefault(_ => _.PhoneNumber == phoneNumber && _.Password == password);
                if (user == null)
                {
                    ErrorData errorData = new ErrorData("Доступ запрещен", "Пользователь не найден");
                    NLog.LogManager.GetCurrentClassLogger().Error("Пользователь не найден!");
                    throw new WebFaultException<ErrorData>(errorData, HttpStatusCode.Unauthorized);
                }
            }
            var res = new UserJson();
            BLToolkit.Mapping.Map.ObjectToObject(user, res);
            res.RoleId = user.Role.Id;
            if(user.CurrentCoordinates != null)
                res.CurrentCoordinatesId = user.CurrentCoordinates.Id;
            return res;
        }

        public List<MarkJson> GetMarksForUser(int userId)
        {
            try
            {
                var repo = new NHibernateRepository<Mark>();
                var marks = repo.GetAll().Where(_ => _.AssignedUser != null && _.AssignedUser.Id == userId && (_.State.Id == (int)MarkStateEnum.Assigned || _.State.Id == (int)MarkStateEnum.InHand || _.State.Id == (int)MarkStateEnum.Accompanied)).ToList();
                marks = marks.OrderByDescending(_ => _.CreateTime).ToList();
                var res = new List<MarkJson>();
                foreach (var m in marks)
                {
                    var mj = new MarkJson();
                    BLToolkit.Mapping.Map.ObjectToObject(m, mj);
                    mj.AssignedUserId = m.AssignedUser.Id;
                    mj.CreateUserId = m.CreateUser.Id;
                    mj.StateId = m.State.Id;
                    mj.CreateTime = m.CreateTime.Ticks;
                    mj.Address = m.Address;
                    res.Add(mj);
                }
                return res;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public bool CreateMark(int userId, double latitude, double longitude, string comment, string address)
        {
            var markRepo = new NHibernateRepository<Mark>();
            var userRepo = new NHibernateRepository<User>();
            var markStateRepo = new NHibernateRepository<MarkState>();
            var allMarks = markRepo.GetAll().Where(x => x.CreateTime.ToLocalTime() >= DateTime.Now.AddHours(-Properties.Settings.Default.DublicatePointIntervalInHours)).ToList();
            foreach (var m in allMarks)
            {
                if (CalcDistance(latitude, longitude, m.Latitude, m.Longitude) <
                    Properties.Settings.Default.DublicatePointIntervalInMeters)
                {
                    var detailedInfo = $"Создаваемая метка дублирует метку по адресу {m.Address}";
                    var errorData = new ErrorData("Создание метки невозможно!", detailedInfo);
                    NLog.LogManager.GetCurrentClassLogger().Error(detailedInfo);
                    throw new WebFaultException<ErrorData>(errorData, HttpStatusCode.NotAcceptable);
                }
            }
            var user = userRepo.Get(userId);
            var ms = markStateRepo.Get(1);
            var mark = new Mark()
            {
                CreateTime = DateTime.UtcNow,
                LastUpdatedTime = DateTime.UtcNow,
                Latitude = latitude,
                Longitude = longitude,
                Comment = comment,
                Address = address,
                CreateUser = user,
                State = ms
            };
            markRepo.Save(mark);
            return true;
        }

        /// <summary>
        /// Расчёт дистанции между двумя точками в метрах
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="lon1"></param>
        /// <param name="lat2"></param>
        /// <param name="lon2"></param>
        /// <returns></returns>
        private int CalcDistance (double lat1, double lon1, double lat2, double lon2)
        {
            var p = 0.017453292519943295;    // Math.PI / 180
            var a = 0.5 - Math.Cos((lat2 - lat1) * p) / 2 +
                    Math.Cos(lat1 * p) * Math.Cos(lat2 * p) *
                    (1 - Math.Cos((lon2 - lon1) * p)) / 2;
            return (int)(12742 * Math.Asin(Math.Sqrt(a)) * 1000); // 2 * R; R = 6371 km
        }

        public List<MarkJson> GetMarks(MarksPeriodEnum period, string beginDate, string endDate)
        {
            try
            {
                var repo = new NHibernateRepository<Mark>();

                List<Mark> marks = null;

                switch (period)
                {
                    case MarksPeriodEnum.All:
                        marks = repo.GetAll().OrderByDescending(x => x.CreateTime).ToList();
                        break;
                    case MarksPeriodEnum.SixHours:
                        marks = repo.GetAll().Where(x => x.CreateTime.ToLocalTime() >= DateTime.Now.AddHours(-6)).OrderByDescending(x => x.CreateTime).ToList();
                        break;
                    case MarksPeriodEnum.Custom:
                        CultureInfo provider = CultureInfo.InvariantCulture;
                        var format = "dd.MM.yyyy";

                        var begin = DateTime.ParseExact(beginDate, format, provider);
                        var end = DateTime.ParseExact(endDate, format, provider);

                        marks = repo.GetAll().Where(x => x.CreateTime.ToLocalTime() >= begin).Where(x => x.CreateTime.ToLocalTime() <= end).OrderByDescending(x => x.CreateTime).ToList();
                        break;
                    default:
                        marks = repo.GetAll().OrderByDescending(x => x.CreateTime).ToList();
                        break;
                }

                var res = new List<MarkJson>();
                foreach (var m in marks)
                {
                    var mj = new MarkJson();
                    
                    mj.Id = m.Id;
                    mj.CreateTime = m.CreateTime.Ticks;
                    //mj.CreateTimeString = m.CreateTime.ToString();
                    mj.Latitude = m.Latitude;
                    mj.Longitude = m.Longitude;
                    mj.AssignedUserId = m.AssignedUser == null ? 0 : m.AssignedUser.Id;//m.AssignedUser.Id;
                    mj.CreateUserId = m.CreateUser.Id;
                    mj.StateId = m.State.Id;
                    mj.Address = m.Address;

                    res.Add(mj);
                }

                return res;
            }
            catch (Exception exception)
            {
                    return null;
            }
        }

        public int GetMarksCount()
        {
            try
            {
                var repo = new NHibernateRepository<Mark>();               

                return repo.Count();
            }
            catch (Exception exception)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(exception.Message);
                return 0;
            }
        }

        public List<UserJson> GetAvarcoms()
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                var users = repo.GetAll().Where(x => x.Role.Id == (int)RoleEnum.Commissar).ToList();

                var res = new List<UserJson>();
                foreach (var u in users)
                {
                    var user = new UserJson();
                    BLToolkit.Mapping.Map.ObjectToObject(u, user);
                    //user.Latitude = u.CurrentCoordinates.Latitude;
                    //user.Longitude = u.CurrentCoordinates.Longitude;
                    res.Add(user);
                }

                return res;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public List<UserJson> GetTaxiDrivers()
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                var users = repo.GetAll().Where(x => x.Role.Id == (int)RoleEnum.Informant).ToList();

                var res = new List<UserJson>();
                foreach (var u in users)
                {
                    var user = new UserJson();
                    BLToolkit.Mapping.Map.ObjectToObject(u, user);

                    res.Add(user);
                }

                return res;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public List<UserJson> GetInformants()
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                //var users = repo.GetAll().Where(x => x.Role.Id == (int)RoleEnum.Informant).ToList();
                //u => new[] { "Admin", "User", "Limited" }.Contains(u.User_Rights
                var users = repo.GetAll().Where(x => new[] { (int)RoleEnum.Informant, (int)RoleEnum.Admin }.Contains(x.Role.Id)).ToList();

                var res = new List<UserJson>();
                foreach (var u in users)
                {
                    var user = new UserJson();
                    BLToolkit.Mapping.Map.ObjectToObject(u, user);

                    res.Add(user);
                }

                return res;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public List<DefaultCommentJson> GetDefaultComments()
        {
            var repo = new NHibernateRepository<DefaultComment>();
            var dcDb = repo.GetAll().ToList();

            var res = new List<DefaultCommentJson>();
            foreach (var d in dcDb)
            {
                var defCom = new DefaultCommentJson();
                BLToolkit.Mapping.Map.ObjectToObject(d, defCom);

                res.Add(defCom);
            }

            return res;
        }

        public UserCordinatesJson GetUserCoords(int userId)
        {
            var res = new UserCordinatesJson();
            var ucRepo = new NHibernateRepository<UserCoordinates>();
            var userCoordsDb = ucRepo.GetAll().FirstOrDefault(_ => _.User.Id == userId);
            if (userCoordsDb != null)
            {
                res.Id = userCoordsDb.Id;
                res.Latitude = userCoordsDb.Latitude;
                res.Longitude = userCoordsDb.Longitude;
                res.UpdateTime = userCoordsDb.UpdateTime.Ticks;
                res.UserId = userCoordsDb.User.Id;
            }
            return res;
        }

        public void CreateOrUpdateUserCoords(UserCordinatesJson usrCoords)
        {
            var ucRepo = new NHibernateRepository<UserCoordinates>();
            var userRepo = new NHibernateRepository<User>();
            var userDb = userRepo.GetAll().FirstOrDefault(_ => _.Id == usrCoords.UserId);
            if (userDb == null)
            {
                return;
                //throw new Exception("Указанного пользователя не существует!");
            }
            UserCoordinates userCoordsDb = null;
            if (userDb.CurrentCoordinates != null)
                userCoordsDb = ucRepo.GetAll().FirstOrDefault(_ => _.Id == userDb.CurrentCoordinates.Id);
            // Если координаты для пользователя уже существуют, то обновим их
            // Иначе заведём новые координаты
            var isNewCoords = false;
            if (userCoordsDb == null)
            {
                userCoordsDb = new UserCoordinates();
                isNewCoords = true;
            }
            userCoordsDb.Latitude = usrCoords.Latitude;
            userCoordsDb.Longitude = usrCoords.Longitude;
            userCoordsDb.UpdateTime = new DateTime(usrCoords.UpdateTime);
            userCoordsDb.User = userDb;
            if (isNewCoords)
                ucRepo.Save(userCoordsDb);
            else
                ucRepo.Update(userCoordsDb);
            userDb.CurrentCoordinates = userCoordsDb;
            userRepo.Update(userDb);
        }

        public void UpdateMark(MarkJson mark)
        {
            try
            {
                var repo = new NHibernateRepository<Mark>();
                var userRepo = new NHibernateRepository<User>();
                var stateRepo = new NHibernateRepository<MarkState>();

                var m = repo.Get(mark.Id);
                m.AssignedUser = userRepo.Get(mark.AssignedUserId);
                m.State = stateRepo.Get(mark.StateId);
                m.LastUpdatedTime = DateTime.UtcNow;
                repo.Update(m);
            }
            catch (Exception exception)
            {
                NLog.LogManager.GetCurrentClassLogger().Info(exception);
                throw;
            }
        }

        //public void UpdateMarks(List<MarkJson> marks)
        //{
        //    try
        //    {
        //        var repo = new NHibernateRepository<Mark>();
        //        var userRepo = new NHibernateRepository<User>();
        //        var stateRepo = new NHibernateRepository<MarkState>();
        //        foreach (var m in marks)
        //        {
        //            var mark = repo.Get(m.Id);
        //            mark.AssignedUser = userRepo.Get(m.AssignedUserId);
        //            mark.State = stateRepo.Get(m.StateId);
        //            mark.LastUpdatedTime = DateTime.UtcNow;
        //            repo.Update(mark);
        //        }
        //    }
        //    catch (Exception exception)
        //    {

        //    }
        //}

        public List<UserJson> GetEmploees()
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                var users = repo.GetAll().ToList();

                var res = new List<UserJson>();
                foreach (var u in users)
                {
                    var user = new UserJson();
                    BLToolkit.Mapping.Map.ObjectToObject(u, user);
                    user.RoleId = u.Role.Id;
                    if(u.CurrentCoordinates != null)
                        user.CurrentCoordinatesId = u.CurrentCoordinates.Id;

                    res.Add(user);
                }

                return res;
            }
            catch (Exception exception)
            {
                return null;
            }
        }

        public void UpdateEmployee(UserJson user)
        {
            NLog.LogManager.GetCurrentClassLogger().Info("UpdateEmployee started!");
            try
            {
                var repo = new NHibernateRepository<User>();
                var roleRepo = new NHibernateRepository<UserRole>();
                NLog.LogManager.GetCurrentClassLogger().Info("user.Id = {0}", user.Id);
                var u = repo.Get(user.Id);
                
                u.Email = user.Email;
                u.FirstName = user.FirstName;
                u.LastName = user.LastName;
                u.Password = user.Password;
                u.Patronymic = user.Patronymic;
                u.PhoneNumber = user.PhoneNumber;
                u.Role = roleRepo.Get(user.RoleId);

                NLog.LogManager.GetCurrentClassLogger().Info("Update beg!");
                repo.Update(u);
                NLog.LogManager.GetCurrentClassLogger().Info("Update fin!");
                
            }
            catch (Exception exception)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(exception);
            }
            NLog.LogManager.GetCurrentClassLogger().Info("UpdateEmployee finished!");
        }

        public UserJson CreateNewEmployee(UserJson newUser)
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                var roleRepo = new NHibernateRepository<UserRole>();
               
                var user = new User();
                user.Email = newUser.Email;
                user.FirstName = newUser.FirstName;
                user.LastName = newUser.LastName;
                user.Password = newUser.Password;
                user.Patronymic = newUser.Patronymic;
                user.PhoneNumber = newUser.PhoneNumber;
                user.Role = roleRepo.Get(newUser.RoleId);
                repo.Save(user);

                var json = new UserJson();
                BLToolkit.Mapping.Map.ObjectToObject(user, json);
                json.RoleId = user.Role.Id;

                return json;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public void DeleteEmployee(UserJson user)
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                var u = repo.Get(user.Id);
                repo.Delete(u);
            }
            catch (Exception exception)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(exception);
            }
        }

        public List<UserCordinatesJson> GetAvarcomCoords(List<int> userIds)
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                var users = repo.GetAll().Where(x => x.Role.Id == (int)RoleEnum.Commissar).Where(x => x.CurrentCoordinates != null).ToList();

                var res = new List<UserCordinatesJson>();
                foreach (var user in users)
                {
                    var json = new UserCordinatesJson();
                    BLToolkit.Mapping.Map.ObjectToObject(user.CurrentCoordinates, json);
                    json.UserId = user.Id;
                    res.Add(json);
                }

                return res;
            }
            catch (Exception exception)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(exception);
                return null;
            }
        }

        public void DeleteMarker(MarkJson mark)
        {
            try
            {
                var repo = new NHibernateRepository<Mark>();
                var u = repo.Get(mark.Id);
                repo.Delete(u);
            }
            catch (Exception exception)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(exception);
            }
        }
    }
}
