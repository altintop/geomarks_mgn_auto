package ru.smart_documents.geoapp;

/**
 * Created by nail on 25.04.2016.
 */
public enum RoleEnum {
    Commissar (1),
    Manager (2),
    TaxiDriver (3);

    private int value;

    private RoleEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
