﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SmartDocuments.GeoMarksDataContracts.DTOs
{
    [DataContract(IsReference = true)]
    public class UserRole
    {
        public UserRole()
        {
            Users = new List<User>();
        }
        [DataMember(Order = 1)]
        public virtual int Id { get; protected set; }
        [DataMember(Order = 2)]
        public virtual string Name { get; set; }
        [DataMember(Order = 3)]
        public virtual string Alias { get; set; }
        [DataMember(Order = 4)]
        public virtual IList<User> Users { get; set; }
    }
}
