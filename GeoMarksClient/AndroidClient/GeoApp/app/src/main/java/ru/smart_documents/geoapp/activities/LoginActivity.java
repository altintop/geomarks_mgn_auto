package ru.smart_documents.geoapp.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import com.loopj.android.http.*;

import cz.msebera.android.httpclient.Header;
import ru.smart_documents.geoapp.GeoAppApplication;
import ru.smart_documents.geoapp.GeoMarksRestClient;
import ru.smart_documents.geoapp.logging.LoggerTask;
import ru.smart_documents.geoapp.RoleEnum;
import ru.smart_documents.geoapp.dtos.UserInfo;
import ru.smart_documents.geoapp.MyProperties;
import ru.smart_documents.geoapp.R;

public class LoginActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private EditText mPhoneNumberView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mSignInButton;
    private CheckBox mLoginWithoutPswdCheckBox;

    private static final String PREFS_NAME = "preferences";
    private static final String PREF_UPHONE_NUMBER = "PhoneNumber";
    private static final String PREF_PASSWORD = "Password";
    private static final String PREF_PSWD_CHECKBOX = "LoginWithoutPswdCheck";
    private final String defaultUnameValue = "";
    private final String defaultPasswordValue = "";
    private final Boolean defaultLoginWithoutPChValue = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        mPhoneNumberView = (EditText) findViewById(R.id.phoneNumber);
        mPhoneNumberView.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        mPasswordView = (EditText) findViewById(R.id.password);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mLoginWithoutPswdCheckBox = (CheckBox) findViewById(R.id.withoutPswdCheckBox);
        mLoginWithoutPswdCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mPasswordView.setEnabled(!isChecked);
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String userNameValue = settings.getString(PREF_UPHONE_NUMBER, defaultUnameValue);
        String passwordValue = settings.getString(PREF_PASSWORD, defaultPasswordValue);
        Boolean loginWithoutPChBValue = settings.getBoolean(PREF_PSWD_CHECKBOX, defaultLoginWithoutPChValue);
        mPhoneNumberView.setText(userNameValue);
        mPasswordView.setText(passwordValue);
        mLoginWithoutPswdCheckBox.setChecked(loginWithoutPChBValue);
        //login
        if(((GeoAppApplication) this.getApplication()).getIsNeedAutoLogin()) {
            ((GeoAppApplication) this.getApplication()).setIsNeedAutoLogin(false);
            // Вызываем сами нажатие кнопки входа
            mSignInButton.callOnClick();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        // Edit and commit
        String userNameValue = mPhoneNumberView.getText().toString();
        String passwordValue = mPasswordView.getText().toString();
        editor.putString(PREF_UPHONE_NUMBER, userNameValue);
        editor.putString(PREF_PASSWORD, passwordValue);
        editor.putBoolean(PREF_PSWD_CHECKBOX, mLoginWithoutPswdCheckBox.isChecked());
        editor.commit();
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        mPhoneNumberView.setError(null);
        mPasswordView.setError(null);

        boolean cancel = false;
        View focusView = null;

        // Store values at the time of the login attempt.
        String phoneNumber = mPhoneNumberView.getText().toString().replaceAll("\\D+","");
        String password = mPasswordView.getText().toString();
        if(mLoginWithoutPswdCheckBox.isChecked())
            password = "";
        else {
            if(TextUtils.isEmpty(password)){
                mPasswordView.setError(getString(R.string.error_field_required));
                focusView = mPasswordView;
                cancel = true;
            }
        }
        /* // Check for a non-empty password.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }*/

        // Check for a valid email address.
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberView.setError(getString(R.string.error_field_required));
            focusView = mPhoneNumberView;
            cancel = true;
        } else if (!isPhoneNumberValid(phoneNumber)) {
            mPhoneNumberView.setError(getString(R.string.error_invalid_phNumber));
            focusView = mPhoneNumberView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            (new LoggerTask("ANDROID__" + phoneNumber, "Пользователь заходит")).execute((Void) null);
            try {
                mAuthTask = new UserLoginTask(phoneNumber, password);
                mAuthTask.execute((Void) null);
            } catch (Exception exc){
                (new LoggerTask("ANDROID__" + phoneNumber, "Ошибка! " + exc.getMessage())).execute((Void) null);
                throw exc;
            }
            (new LoggerTask("ANDROID__" + phoneNumber, "Пользователь зашел")).execute((Void) null);
        }
    }

    private boolean isPhoneNumberValid(String phoneNumber) {
        //TODO: Replace this with your own logic
        return (phoneNumber.matches("[0-9]+") && phoneNumber.length() == 10);
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Метод открывает следующую после логирования активность
     * Если текущий юзер аварком, то открываем активность аваркома,
     * Иначе открываем активность таксиста
     */
    private void navigateToControlActivity(){
        Intent controlIntent = null;
        UserInfo userInfo = MyProperties.getInstance().userInfo;
        if(userInfo.getRoleId() == RoleEnum.Commissar.getValue()) {
            controlIntent = new Intent(getApplicationContext(),CommissarControlActivity.class);
        } else {
            controlIntent = new Intent(getApplicationContext(),TaxiControlActivity.class);
        }
        startActivity(controlIntent);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mPhNumber;
        private final String mPassword;

        UserLoginTask(String phoneNumber, String password) {
            mPhNumber = phoneNumber;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.
            // Здесь лезем в сервис с нужными параметрами и проверяем существование юзера
            // 1) Если есть == возвращаем true
            // 2) Если нет == возвращаем false
            // Make RESTful webservice call using AsyncHttpClient object
            final boolean[] result = {false};
            GeoMarksRestClient client = new GeoMarksRestClient();
            RequestParams rp = new RequestParams();
            rp.put("phoneNumber", mPhNumber);
            rp.put("password", mPassword);
            client.get("ValidateUser", rp, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    UserInfo userInfo = MyProperties.getInstance().userInfo;
                    try
                    {
                        userInfo.setId(response.getInt("Id"));
                        userInfo.setPhoneNumber(response.getString("PhoneNumber"));
                        userInfo.setEmail(response.getString("Email"));
                        userInfo.setFirstName(response.getString("FirstName"));
                        userInfo.setPassword(response.getString("Password"));
                        userInfo.setLastName(response.getString("LastName"));
                        userInfo.setPatronymic(response.getString("Patronymic"));
                        userInfo.setRoleId(response.getInt("RoleId"));
                    } catch (JSONException exc) {}
                    result[0] = true;
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if(statusCode == 503){
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }});
                    }
                    else if(statusCode == 400){
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Пользователь не существует или пароль неверен", Toast.LENGTH_LONG).show();
                            }});
                    }
                    // When Http response code other than 400, 503
                    else{
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }});
                    }
                    result[0] = false;
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if(statusCode == 503){
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }});
                    }
                    else if(statusCode == 400 || statusCode == 401){
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Пользователь не существует или пароль неверен", Toast.LENGTH_LONG).show();
                            }});
                    }
                    // When Http response code other than 400, 503
                    else{
                        LoginActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }});
                    }
                    result[0] = false;
                }
            });
            return result[0];
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                navigateToControlActivity();
            } else {
                //mPasswordView.setError(getString(R.string.error_incorrect_password));
                //mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}
