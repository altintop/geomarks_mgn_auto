-- Table: "mark_state"

-- DROP TABLE mark_state;

CREATE TABLE mark_state
(
  id integer NOT NULL, -- ������������� �������� �������
  name character varying(30) NOT NULL, -- �������� ������� �� ������� �����
  alias character varying(30) NOT NULL, -- �������� �������� �������
  CONSTRAINT pk_mark_state_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mark_state
  OWNER TO postgres;
COMMENT ON TABLE mark_state
  IS '�������-���������� �������� �������';
COMMENT ON COLUMN mark_state.id IS '������������� �������� �������';
COMMENT ON COLUMN mark_state.name IS '�������� ������� �� ������� �����';
COMMENT ON COLUMN mark_state.alias IS '�������� �������� �������';

