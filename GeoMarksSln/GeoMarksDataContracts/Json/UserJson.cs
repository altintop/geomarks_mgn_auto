﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksDataContracts.Json
{
    [DataContract]
    public class UserJson
    {
        [DataMember(Order = 1)]
        public virtual int Id { get; set; }
        [DataMember(Order = 2)]
        public virtual string PhoneNumber { get; set; }
        [DataMember(Order = 3)]
        public virtual string Email { get; set; }
        [DataMember(Order = 4)]
        public virtual string FirstName { get; set; }
        [DataMember(Order = 5)]
        public virtual string Password { get; set; }
        [DataMember(Order = 6)]
        public virtual string LastName { get; set; }
        [DataMember(Order = 7)]
        public virtual string Patronymic { get; set; }
        [DataMember(Order = 8)]
        public virtual int RoleId { get; set; }
        [DataMember(Order = 9)]
        public virtual int CurrentCoordinatesId { get; set; }
    }
}
