'use strict';

var myApp = angular.module('myApp');
myApp.service('authorizationService', ['$http', '$q', 'appSettings', function($http, $q, appSettings) {

	  //var authorizedUser = null;

	  var loginUrl = appSettings.baseUrl + 'validateuser';

    var setUser = function(user) {
      localStorage["gm-authorized-user"] = angular.toJson(user);
    };

    var getUser = function() {
      var user = localStorage["gm-authorized-user"];      
      if (user === null) {
        return null;
      }
      
      return angular.fromJson(user); 
    };
  	
    var resetUser = function() {
      localStorage.removeItem("gm-authorized-user");
    };

    this.userId = function() {
      var user = getUser();
      if (user !== null) {
        return user.Id;
      } else { 
        return -1;
      }
    };

  	this.login = function(userLogin, userPassword) {
    	return $http.get(loginUrl, { params: { phoneNumber: userLogin, password: userPassword } })
                .then(function(response) {
                    if (typeof response.data === 'object') {
                        //authorizedUser = response.data;
                        setUser(response.data);
                        return true;                            
                    } else 
                    {
                    // invalid response
                        return false;//$q.reject(response.data);
                    }
                }, function() {
                    // something went wrong
                    return false;//$q.reject(response.data);
                });
  	};

  	this.getAuthorizedUser = function() {
  		return getUser();
  	};

  	this.isAuthorized = function() {
      var user = getUser();
  		return (user !== null)&&(user !== undefined);
  	};

  	this.checkStateRights = function(linkName) {
      var user = getUser();

	    if (user === null) return false;

      if (user === undefined) return false;

	    if (user.RoleId == appSettings.userRoles.ADMIN) {
	      return true;
	    }
	    
	    if ((user.RoleId == appSettings.userRoles.MANAGER) && (linkName == 'employees')){
	      return false;
	    }

      if ((user.RoleId == appSettings.userRoles.MANAGER) && (linkName == 'stat')){
        return false;
      }

	    if ((user.RoleId == appSettings.userRoles.MANAGER) && (linkName == 'map')){
	      return true;
	    }

	    return false;
  	};

  	this.logOut = function() {
  		resetUser();	
  	};  	
}]);