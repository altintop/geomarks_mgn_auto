﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksDataContracts.Json
{
    [DataContract]
    public class MarkJson
    {
        [DataMember(Order = 1)]
        public virtual int Id { get; set; }
        [DataMember(Order = 2)]
        public virtual double Longitude { get; set; }
        [DataMember(Order = 3)]
        public virtual double Latitude { get; set; }
        [DataMember(Order = 4)]
        public virtual long CreateTime { get; set; }
        [DataMember(Order = 5)]
        public virtual Int32 CreateUserId { get; set; }
        [DataMember(Order = 6)]
        public virtual Int32 AssignedUserId { get; set; }
        [DataMember(Order = 7)]
        public virtual Int32 StateId { get; set; }
        [DataMember(Order = 8)]
        public virtual string LastUpdatedTime { get; set; }
        [DataMember(Order = 9)]
        public virtual string Comment { get; set; }
        [DataMember(Order = 10)]
        public virtual string Address { get; set; }
        //[DataMember(Order = 11)]
        //public virtual string CreateTimeString { get; set; }
    }
}
