﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksModel.Mappers
{
    public class DefaultCommentMap : ClassMap<DefaultComment>
    {

        public DefaultCommentMap()
        {
            Table("default_comment");

            Id(x => x.Id).GeneratedBy.Identity().Column("id");

            Map(x => x.Text).Column("text").Not.Nullable();
            Map(x => x.Alias).Column("alias").Not.Nullable();
        }
    }
}
