﻿using System.Runtime.Serialization;

namespace SmartDocuments.GeoMarksDataContracts
{
    /// <summary>
    /// Enum отвечающий за состояние отметок
    /// </summary>
    [DataContract]
    public enum MarksPeriodEnum
    {
        [EnumMember]
        All = 1,
        [EnumMember]
        SixHours = 2,
        [EnumMember]
        Custom = 3
    }
}
