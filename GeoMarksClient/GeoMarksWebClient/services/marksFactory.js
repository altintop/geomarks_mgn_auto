'use strict';

var myApp = angular.module('myApp');
myApp.factory('marksFactory', ['$http', '$q', 'appSettings', function($http, $q, appSettings) {

    var urlBase = appSettings.baseUrl;
    var urlMarkers = urlBase + 'marker/';
    var urlAvarcoms = urlBase + 'avarcom/';
    var urlTaxiDrivers = urlBase + 'taxidriver/';
    var urlDeleteMark = urlBase + 'delete-marker/'
    var urlCreateMark = urlBase + 'createmark/?userId={0}&latitude={1}&longitude={2}&comment={3}&address={4}';
    var urlMarkersCount = urlBase + 'marker/count/';

    var geocoder = new google.maps.Geocoder;

    var factory = {};

    factory.markersCount = 0;

    factory.getAllTaxiDrivers = function() {
            return $http.get(urlTaxiDrivers);                
        };

    factory.getAllAvarcoms = function() {
        return $http.get(urlAvarcoms);
    }; 
 
    factory.checkForNewMarks = function() {
      var url = urlMarkersCount;
      return $http.get(url)
                .then(function(response) {
                    var res = 0;

                    if(factory.markersCount !== 0)
                    {
                       res = factory.markersCount < response.data ? response.data - factory.markersCount : 0;                      
                    }

                    factory.markersCount = response.data;

                    return res;
                }, function(response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
    };

    factory.setAddress = function(latitude, longitude, callback, item) {
        var marker = new google.maps.LatLng(latitude, longitude);
        
        geocoder.geocode({'location': marker}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                callback(results[0].formatted_address, item);
            }
        });        
    };

    function getMMDDYY(ticks) {
      var date = new Date(ticks);
      var mm = date.getMonth()+1;
      var dd = date.getDate();
      var yy = new String(date.getFullYear()).substring(2);
      if (mm < 10) mm = "0"+mm;
      if (dd < 10) dd = "0"+dd;
      return ""+mm+dd+yy;
    }

    function getDDMMYYYYString(dt) {
      if(dt === null)
      {
        return null;
      }
      var dd = dt.getDate();
      var mm = dt.getMonth()+1; //January is 0!
      var yyyy = dt.getFullYear();

      if(dd < 10) {
          dd = '0' + dd;
      }

      if(mm < 10) {
          mm = '0' + mm;
      } 

      return dd + '.' + mm + '.' + yyyy;
    }

    factory.getMarks = function(period, beginDate, endDate) {
            var begin = getDDMMYYYYString(beginDate);
            var end = getDDMMYYYYString(endDate);

            return $http.get(urlMarkers, {params: { period: period, beginDate: begin, endDate: end }})
                .then(function(response) {
                    if (typeof response.data === 'object') {
                        //response.data.forEach(function(item, i, arr) {
                          //item.CreateTimeString = getMMDDYY(item.CreateTime);
                        //});
                            return response.data;                            
                    } else 
                    {
                    // invalid response
                        console.log('could not get all marks' + response.data);
                        return $q.reject(response.data);
                    }
                }, function(response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
        
        };

    String.prototype.format = String.prototype.f = function(){
        var args = arguments;
        return this.replace(/\{(\d+)\}/g, function(m,n){
          return args[n] ? args[n] : m;
        });
    };
//"/CreateMark/?userId={userId}&latitude={latitude}&longitude={longitude}&comment={comment}&address={address}"
    factory.createMark = function(userId, latitude, longitude, comment, address) {
      var url = urlCreateMark.f(userId, latitude, longitude, comment, address);
      return $http.get(url)
                .then(function(response) {
                    //$q.resolve();
                    return response;
                }, function(response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
    }


/*
    factory.saveMarks = function(arr){
        return $http
        ({
                   url: urlBase + 'marker/',
                   method: 'POST',
                   headers: { 'Content-Type':'application/json; charset=utf-8'},
                   data: JSON.stringify(arr)
        })
    }
*/
    factory.deleteMark = function(obj) {
        var res;
        return res = $http
        ({
                   url: urlDeleteMark,
                   //method: 'DELETE',
                   method: 'POST',
                   headers: { 'Content-Type':'application/json; charset=utf-8'},
                   data: JSON.stringify(obj)
        })
        .then(function(response) {
            return true;       
        }, 
        function(response) { // optional
            return false;
        });
    }

    factory.updateMark = function(obj){
        return $http
        ({
                   url: urlBase + 'marker/',
                   method: 'POST',
                   headers: { 'Content-Type':'application/json; charset=utf-8'},
                   data: JSON.stringify(obj)
        })
    }    

    factory.getAvarcomsUrl =  function() {
        return urlAvarcoms;
    };

    factory.getTaxiDriversUrl = function() {
        return urlTaxiDrivers;
    };

    return factory;
}]);