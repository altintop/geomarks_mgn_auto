'use strict';

myApp = angular.module('myApp')
myApp.controller('statViewController', ['$scope', 'marksFactory', 'employeesFactory', '$http', '$location', 'authorizationService', 'appSettings', function ($scope, marksFactory, employeesFactory, $http, $location, authorizationService, appSettings) {
/*
    if (authorizationService.checkStateRights('stat') == false) {
        $location.path('/denied');
    }
*/
    var taxiDrivers = [];

    var grid = null;

    employeesFactory.getAllTaxiDrivers().then(function(res) {
        taxiDrivers.length = 0;
        res.forEach(function(item, i, arr) {
                taxiDrivers.push(item);
            });        
    });

    var states = [{
            "ID": 1,
            "Name": "Новое",
            "Color": "#FF4040"
        }, {
            "ID": 2,
            "Name": "Назначена",
            "Color": "#5CCCCC"
        }, {
            "ID": 3,
            "Name": "Сопровождается",
            "Color": "#FF7400"
        },
        {
            "ID": 4,
            "Name": "Завершена",
            "Color": "#67E667"
        },
        {
            "ID": 5,
            "Name": "В работе",
            "Color": "#FFFFFF"
        },
        {
            "ID": 6,
            "Name": "Отказался",
            "Color": "#FFFFFF"
        },
        {
            "ID": 7,
            "Name": "Спор",
            "Color": "#FFFFFF"
        },
        {
            "ID": 8,
            "Name": "Пострадавший",
            "Color": "#FFFFFF"
        },
        {
            "ID": 9,
            "Name": "Скрылся",
            "Color": "#FFFFFF"
        },
        {
            "ID": 10,
            "Name": "Ложная",
            "Color": "#FFFFFF"
        },
        {
            "ID": 11,
            "Name": "Опоздал",
            "Color": "#FFFFFF"
        }];

	$scope.gridOptions = {
            editing: {
                //editEnabled: true,
                //editMode: 'row',
                //insertEnabled: true,
                //removeEnabled: true
            },

            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true
            },
            headerFilter: {visible:true},
            grouping: {
               autoExpandAll: false
            },
            groupPanel: {
                visible: true
            },
            "export": {
                enabled: true,
                fileName: "Employees",
                //allowExportSelectedData: true
            },
            bindingOptions: {
                dataSource: 'marks',
                autoExpandAll: true,
            },

            columns: [
                {
                    dataField: "Id",
                    caption: "№",
                    width: 40,
                    allowEditing: false 
                },
                {
                    dataField: "CreateUserId",
                    caption: "Номер телефона",
                    groupIndex: 0,
                    lookup: {
                        dataSource: new DevExpress.data.CustomStore({
                            load: function (loadOptions) {
                                var deferred = new $.Deferred();
                                
                                if (taxiDrivers == null) {
                                    employeesFactory.getAllTaxiDrivers().then(function(response){
                                        taxiDrivers = response;
                                        deferred.resolve(response);
                                    });    

                                } else {
                                    deferred.resolve(taxiDrivers);    
                                };


                                return deferred.promise();
                            }
                        }),
                        valueExpr: 'Id', displayExpr: 'PhoneNumber',                        
                    }    
                },   
                {
                    dataField: "Address",                    
                    //width: 150,
                    caption: "Отметка"                    
                },
                {
                    dataField: "StateId",                    
                    //width: 150,
                    caption: "Состояние",
                    lookup: {
                        dataSource: states,
                        displayExpr: "Name",
                        valueExpr: "ID"
                    },
                    filterValue: 4,
                },
                {
                    dataField: "dt",
                    dataType: "date",
                    caption: "Время ДТП",
                    allowEditing: false,
                    format: 'dd.MM.yyyy HH:mm:ss',
                    selectedFilterOperation:'='
                },

            ],
             summary: {
                groupItems: [{
                    column: "Id",
                    summaryType: "count",                    
                    displayFormat: "Количество отметок: {0}",
                    //showInGroupFooter: true,
                }]
            },
            //paging: { pageSize: 20 },
            //scrolling: {
            //      mode: 'infinite'
            //},
            contentReadyAction: function(e) {
                                   grid = e.component;
                                   //rid.collapseAll(0);
                                },
           
        };

    $scope.beginDate = new Date();
    $scope.beginDate.setDate($scope.beginDate.getDate()-7);
    $scope.endDate = new Date();    

    $scope.beginDateBox = {
        eventOptions: {
            applyValueMode: "useButtons",
            formatString: "dd.MM.yyyy",            
            value: $scope.beginDate,
            onValueChanged: function(data) {
                $scope.beginDate = new Date(data.value);
                $scope.loadData();                
            }
        }
    };

    $scope.endDateBox = {
        eventOptions: {
            applyValueMode: "useButtons",
            formatString: "dd.MM.yyyy",
            value: $scope.endDate,
            onValueChanged: function(data) {
                $scope.endDate = new Date(data.value);
                $scope.loadData();                
            }
        }
    };

    $scope.formatDate = function(dt) {
        Date.prototype.addHours= function(h){
            this.setHours(this.getHours()+h);
            return this;
        };
        //1) вычесть 621355968000000000L
        //2) Поделить на 10000 -- получить миллисекунды
        //3) Вызвать конструктор для Date(миллисекунды) и передать туда полученные миллисекунды в п.2
        var d = dt - 621355968000000000;
        d = d / 10000;        
 
       
        var tickDate = new Date(d);
        //var offset = tickDate.getTimezoneOffset();
        //var hOffset = offset/60;
        //tickDate = tickDate.addHours(hOffset);
        
        return tickDate;
    };

    // Получение всех отметок
    $scope.loadData = function(){
        marksFactory.getMarks(appSettings.marksPeriod.CUSTOM, $scope.beginDate, $scope.endDate).then(function(response){
            $scope.marks = response;
            if (grid) { 
                grid.refresh();
                grid.collapseAll(0);
            }
                
            $scope.marks.forEach(function(item, i, marks) {              
                item.dt = $scope.formatDate(item.CreateTime);
            });      
        });
    };

    $scope.loadData();
}]);