﻿'use strict';

﻿angular.module('myApp', ['ngResource', 'dx', 'ui.router']) //, 'uiGmapgoogle-maps'

.config(function ($httpProvider, $stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise("/login");
  
  $stateProvider
    .state('loginState', {
      url: "/login",
      templateUrl: "views/loginView.html",
      controller: 'loginViewController'
    })    
    .state('mapState', {
      url: "/map",
      templateUrl: "views/mapView.html",
      controller: 'mapViewController'
    })
    .state('employeesState', {
      url: "/employees",
      templateUrl: "views/employeesView.html",
      controller: 'employeesViewController'
    })
    .state('accessDeniedState', {
      url: "/denied",
      templateUrl: "views/accessDeniedView.html"      
    })
    .state('statState', {
      url: "/statistics",
      templateUrl: "views/statView.html",      
      controller: 'statViewController'
    });
})

.constant("appSettings", {
        //"baseUrl": "http://213.59.129.160:8081/GeoMarksRest/GeoMarksRestSvc.svc/"
        //"baseUrl": "http://localhost:49545/GeoMarksRestSvc.svc/",
        "baseUrl": "http://195.151.216.17:8081/GeoMarksRest/GeoMarksRestSvc.svc/",
        marksPeriod: {
            "ALL": 1,
            "SIX_HOURS": 2,
            "CUSTOM": 3           
        },
        userRoles: {
            "AVARCOM": 1,
            "MANAGER": 2,
            "TAXI_DRIVER": 3,
            "ADMIN": 4            
        },
        mapConfig: {
           "LATITUDE": 55.176076,
           "LONGITUDE": 61.406506 
        }
    })

.controller('mainController', ['$scope', '$http', 'authorizationService', 'marksFactory', '$location', function ($scope, $http, authorizationService, marksFactory, $location) { 

  $scope.hasRights = function(linkName) {  
    return authorizationService.checkStateRights(linkName);   
  };

  $scope.defineVisibility = function(linkName) {
    switch (linkName) {
      case '/map':
        $scope.mapClass = 'active';
        $scope.employeeClass = '';
        $scope.statClass = '';
        break;
      case '/employees':
        $scope.mapClass = '';
        $scope.employeeClass = 'active';
        $scope.statClass = '';
        break;
      case '/statistics':
        $scope.mapClass = '';
        $scope.employeeClass = '';
        $scope.statClass = 'active';
        break;   
      default:
        $scope.mapClass = 'active';
        $scope.employeeClass = '';
    }
  };

  $scope.isAuthorized = function() {
    var user = authorizationService.getAuthorizedUser();
    return (user !== null)&&(user !== undefined);
  };

  $scope.logOut = function() {
    authorizationService.logOut();
    $location.path('login');
  };


  var tab = $location.path();
  $scope.defineVisibility(tab);

  setInterval(function() {
    marksFactory.checkForNewMarks().then(function(res) {      
      if (res > 0) playSound();
    });
  }, 1000*20);

  function playSound() {
    var audio = new Audio(); // Создаём новый элемент Audio
    audio.src = 'audio/sound.mp3'; // Указываем путь к звуку "клика"
    audio.autoplay = true; // Автоматически запускаем
  }

}]);