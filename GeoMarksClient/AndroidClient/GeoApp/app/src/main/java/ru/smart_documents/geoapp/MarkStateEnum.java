package ru.smart_documents.geoapp;

/**
 * Created by nail on 17.04.2016.
 */
public enum MarkStateEnum {
    New (1),
    Assigned (2),
    Accompanied (3),
    Completed (4),
    InHand (5),
    Refused (6),
    Dispute (7),
    Hurt (8),
    Disappeared (9),
    Mistaken (10),
    Late (11);


    private int value;

    private MarkStateEnum(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
