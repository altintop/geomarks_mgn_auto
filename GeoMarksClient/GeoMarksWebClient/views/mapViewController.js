'use strict';

myApp = angular.module('myApp');

myApp.controller('mapViewController', ['$scope', 'marksFactory', 'employeesFactory', '$http', 'authorizationService', '$location', 'appSettings', function ($scope, marksFactory, employeesFactory, $http, authorizationService, $location, appSettings) {

    if (authorizationService.checkStateRights('map') === false) {
        $location.path('/denied');
    }
    

    var states = [{
            "ID": 1,
            "Name": "Новое",
            "Color": "#FF4040",
            "Icon": "images/red_MarkerA.png"
        }, {
            "ID": 2,
            "Name": "Назначена",
            "Color": "#5CCCCC",
            "Icon": "images/paleblue_MarkerH.png"
        }, {
            "ID": 3,
            "Name": "Сопровождается",
            "Color": "#FF7400",
            "Icon": "images/orange_MarkerC.png"
        },
        {
            "ID": 4,
            "Name": "Завершена",
            "Color": "#67E667",
            "Icon": "images/grey_MarkerX.png"
        },
        {
            "ID": 5,
            "Name": "В работе",
            "Color": "#FFFFFF",
            "Icon": "images/grey_MarkerX.png"
        },
        {
            "ID": 6,
            "Name": "Отказался",
            "Color": "#FFFFFF",
            "Icon": "images/grey_MarkerX.png"
        },
        {
            "ID": 7,
            "Name": "Спор",
            "Color": "#FFFFFF",
            "Icon": "images/grey_MarkerX.png"
        },
        {
            "ID": 8,
            "Name": "Пострадавший",
            "Color": "#FFFFFF",
            "Icon": "images/grey_MarkerX.png"
        },
        {
            "ID": 9,
            "Name": "Скрылся",
            "Color": "#FFFFFF",
            "Icon": "images/grey_MarkerX.png"
        },
        {
            "ID": 10,
            "Name": "Ложная",
            "Color": "#FFFFFF",
            "Icon": "images/grey_MarkerX.png"
        },
        {
            "ID": 11,
            "Name": "Опоздал",
            "Color": "#FFFFFF",
            "Icon": "images/grey_MarkerX.png"
        }];
    
    var grid = null;

    $scope.marks = [];
    $scope.markers = [];


    var employees = [];
    var taxiDrivers = [];
    var avarcoms = [];

    var statuses = [{id:"all", value:"Все"}, {id:"sixHours", value:"За 6 часов"}];
    $scope.showPeriod = appSettings.marksPeriod.SIX_HOURS;


    employeesFactory.getAllTaxiDrivers().then(function(res) {
        taxiDrivers.length = 0;
        res.forEach(function(item, i, arr) {
                taxiDrivers.push(item);
            });

        employeesFactory.getAllAvarcoms().then(function(response) {
            avarcoms.length = 0;
            response.forEach(function(item, i, arr) {
                avarcoms.push(item);
            });

            $scope.getData(false, $scope.showPeriod);
            setInterval(function() {
                $scope.getData(true, $scope.showPeriod);
            }, 1000*60);
        });
    });

    
    $scope.selectStatusOptions = {
        dataSource: statuses,
        value: 'sixHours',
        valueExpr: 'id',
        displayExpr: 'value',
        onValueChanged: function(data) {
            if (data.value == "all")
                $scope.showPeriod = appSettings.marksPeriod.ALL;               
            else
                if (grid) {
                   $scope.showPeriod = appSettings.marksPeriod.SIX_HOURS;
                }

            $scope.getData(false, $scope.showPeriod);

            if (grid) {
                grid.refresh();   
            }              
        }
    };

    // Описание грида
    $scope.gridOptions = {
        /*
            width: function () { 
                var baseWidth = $( "#mark-grid-parent" ).width();
                return baseWidth;
            },
        */
            allowColumnResizing: true,
            allowColumnReordering: true,
            editing: {
                        editMode: 'cell',
                        editEnabled: true,
                        //allowUpdating: true,
                        //allowDeleting: true,
                        //allowAdding: true
                        removeEnabled: true
                    },

            bindingOptions: {
                dataSource: 'marks',            
            },
            groupPanel: {
                visible: true
            },
            filterRow: {
                visible: false
            },
            headerFilter: {visible:true},
            columns: [
                {
                    dataField: "Id",
                    caption: "№",
                    width: 40,
                    allowEditing: false 
                },
                {
                    dataField: "Address",
                    caption: "Адрес",
                    //width: 'auto',
                    allowEditing: false 
                },   
                {
                    dataField: "dt",
                    dataType: "date",
                    caption: "Время ДТП",
                    allowEditing: false,
                    format: 'dd.MM.yyyy HH:mm:ss',
                    selectedFilterOperation:'='
                }, 
                {
                    dataField: "CreateUserId",
                    caption: "Автор отметки",
                    //width: 'auto',
                    allowEditing: false,
  

                    lookup: {
                        dataSource: new DevExpress.data.CustomStore({
                            load: function (loadOptions) {
                                var deferred = new $.Deferred();
                                
                                if (taxiDrivers == null) {
                                    employeesFactory.getAllTaxiDrivers().then(function(response){
                                        taxiDrivers = response;
                                        deferred.resolve(response);
                                    });    

                                } else {
                                    deferred.resolve(taxiDrivers);    
                                };


                                return deferred.promise();
                            }
                        }),
                        valueExpr: 'Id', displayExpr: 'LastName'
                    }
                },

                 {
                    dataField: "CreateUserId",
                    caption: "Телефон",
                    //width: 'auto',
                    allowEditing: false,
  

                    lookup: {
                        dataSource: new DevExpress.data.CustomStore({
                            load: function (loadOptions) {
                                var deferred = new $.Deferred();
                                
                                if (taxiDrivers == null) {
                                    employeesFactory.getAllTaxiDrivers().then(function(response){
                                        taxiDrivers = response;
                                        deferred.resolve(response);
                                    });    

                                } else {
                                    deferred.resolve(taxiDrivers);    
                                };


                                return deferred.promise();
                            }
                        }),
                        valueExpr: 'Id', displayExpr: 'PhoneNumber'
                    }
                },

                {
                    dataField: "AssignedUserId",
                    caption: "Аварком",
                    //width: 'auto',


                     lookup: {
                        dataSource: new DevExpress.data.CustomStore({
                            load: function (loadOptions) {

                                var deferred = new $.Deferred();
                                
                                if (avarcoms == null) {                                   
                                    employeesFactory.getAllAvarcoms().then(function(response){
                                        avarcoms = response;
                                        deferred.resolve(response);
                                    });    

                                } else {
                                    deferred.resolve(avarcoms);    
                                };                             

                                return deferred.promise();
                            }
                        }),
                        valueExpr: 'Id', displayExpr: 'LastName'
                    }


                },               
                {
                    dataField: "StateId",
                    caption: "Состояние",
                    //width: 'auto',
                    //allowEditing: false 
                    
                    lookup: {
                        dataSource: states,
                        displayExpr: "Name",
                        valueExpr: "ID"
                    }                

                },                
                {
                    dateField: "Latitude",
                    visible: false
                },
                {
                    dateField: "Longitude",
                    visible: false
                }                
            ],
            summary: {
                groupItems: [{
                    column: "Id",
                    summaryType: "count",                    
                    displayFormat: "Суммарное количество отметок: {0}",
                    //showInGroupFooter: true,
                }]
            },
            rowPrepared: function(rowElement, rowInfo) {
                            if (rowInfo != null) {
                                if (rowInfo.rowType == "data") {
                                    if(rowInfo.data != null) {
                                        rowElement.css('background', states[rowInfo.data.StateId - 1].Color);    
                                    }                              
                                }  
                            }                            
                            //else if (rowInfo.data.StateId == 1)
                            //     rowElement.css('background', states[rowInfo.data.StateId].Color);                            
            },
            contentReadyAction: function(e) {
                grid = e.component;                
            },
            
            onRowUpdated: function(e) {              
                //marksFactory.saveMarks($scope.marks);                
                if (e != null) {
                    if (e.key != null) {
                        marksFactory.updateMark(e.key).then(function() {
                            $scope.getData(true, $scope.showPeriod);
                        });
                        //$scope.reDrawMap();
                    }                                
                }
            },

            onRowUpdating: function(e) {
                if((e.oldData.AssignedUserId == 0)&&(e.newData.AssignedUserId != 0)) {
                    e.newData.StateId = 2;
                };

                 if((e.oldData.StateId != 1)&&(e.newData.StateId == 1)) {
                    e.cancel = true;
                    $scope.popupTitle = 'Нельзя выставить статус "Новая" для уже назначенной заявки';
                    $scope.visiblePopup = true;
                    grid.refresh();
                };

            },

            onInitialized: function(e) {
                grid = e.component;
            },

            editingStart: function(e) {

                if ((e.column.dataField == 'StateId')&&(e.data.AssignedUserId == 0)) {
                    $scope.popupTitle = 'Прежде чем менять статус заявки, необходимо назначить сотрудника';
                    $scope.visiblePopup = true;
                    e.cancel = true;
                }
            },
            onRowRemoving: function(e) {
                marksFactory.deleteMark(e.key).then(function(){
                    $scope.getData(true, $scope.showPeriod);
                });                
            },

        };
    

    $scope.reDrawMap = function() {
        //$scope.clearMarkers();
        $scope.marks.forEach(function(item, i, marks) {
                var marker = new google.maps.LatLng(item.Latitude, item.Longitude);
                if (item.StateId != 4) {
                    $scope.addMarker(marker, '№' + item.Id, states[item.StateId - 1].Icon);    
                }
            });
    }

  // Получение всех отметок
    $scope.getData = function(update, period) {

        $scope.clearMarkers();

        marksFactory.getMarks(period, null, null).then(function(response){
            $scope.marks = response;
            if (grid) {
                grid.refresh();   
            }
            
            $scope.marks.forEach(function(item, i, marks) {               
                item.dt = $scope.formatDate(item.CreateTime);
            });

            $scope.reDrawMap();

            if (!update) {
                var center = $scope.calcMapCenter();
                if (($scope.marks != null) && ($scope.marks.length != 0)) {
                    $scope.map.setCenter(new google.maps.LatLng(center.Latitude, center.Longitude));             
                }
            }
        });

        employeesFactory.getCoords(avarcoms).then(function(response) {            
            response.forEach(function(item, i, arr) {
                // убираем аваркома с кодом 0
                if (item.UserId === 0) {
                    return false;
                }

                var marker = new google.maps.LatLng(item.Latitude, item.Longitude);
                var title = '';
                avarcoms.forEach(function(user, i, arr) {                    
                    if (user.Id == item.UserId) {
                        title = user.LastName + ' ' + user.FirstName;
                    }
                });
                $scope.addAvarcomMarker(marker, title);
            });            
        });

        
        if (grid) {
            grid.refresh();   
        }
    };


    // Инициализация карты
    //55.175291, 61.466244
    var latitude = appSettings.mapConfig.LATITUDE;
    var longitude = appSettings.mapConfig.LONGITUDE;

    var haightAshbury = new google.maps.LatLng(latitude, longitude);//(долгота, широта)
        $scope.map = new google.maps.Map(document.getElementById('map'), {
            center: haightAshbury,
            zoom: 10
        });

    // инициализация поисковой строки адреса
    var input = document.getElementById('searchTextField');

    var options = {
        //bounds: defaultBounds,
        types: ['address'],
        componentRestrictions: {country: 'ru'}
    };

    $scope.autocomplete = new google.maps.places.Autocomplete(input, options);

    var circle = new google.maps.Circle({
        center: new google.maps.LatLng(latitude, longitude), //
        radius: 1000
      });
    
    $scope.autocomplete.setBounds(circle.getBounds());

    $scope.addMark = function() {
        var place = $scope.autocomplete.getPlace();        
        var coords = place.geometry.location;
        var latitude = coords.lat();
        var longitude = coords.lng();
        var userId = authorizationService.userId();
        if (userId == -1 ) return;        
        
        marksFactory.createMark(userId, latitude, longitude, 'не задан', place.name).then(function(res) {
            $scope.getData(false, $scope.showPeriod);
        });      
    }

    $scope.calcMapCenter = function() {

        var maxX = 0, minX = 100000, maxY = 0, minY = 100000;
        var count = 0;
        $scope.marks.forEach(function(item, i, marks) {
                if (item.StateId != 4) {
                    if (item.Latitude > maxX ) maxX = item.Latitude;
                    if (item.Latitude < minX ) minX = item.Latitude;

                    if (item.Longitude > maxY ) maxY = item.Longitude;
                    if (item.Longitude < minY ) minY = item.Longitude;
                    count++;
                }                                            
            });
        
         var center = {};
        //проверка если все заявки завершены
        if (count == 0) {
            center.Latitude = appSettings.mapConfig.LATITUDE;
            center.Longitude = appSettings.mapConfig.LONGITUDE;
            return center; 
        } 

        var koeff = 0.0001;

       
        minX = minX - koeff;
        maxX = maxX + koeff;

        minY = minY - koeff;
        maxY = maxY + koeff;

        center.Latitude = minX + (maxX - minX) / 2;
        center.Longitude = minY + (maxY - minY) / 2;        

        return center;
    };

    // Добавление отметок на карту
    $scope.addMarker = function (location, text, icon) {
                //var image = icon;
                var marker = new google.maps.Marker({
                    position: location,
                    map: $scope.map,
                    //shadow: shadow,
                    //icon: image,
                    title: text,
                    zIndex: 999,
                    icon: icon
                });//добавление маркера

                $scope.markers.push(marker);
            };

    $scope.addAvarcomMarker = function(location, text) {
                var image = 'images/blue_MarkerA.png'; 
                var marker = new google.maps.Marker({
                    position: location,
                    map: $scope.map,
                    //shadow: shadow,
                    //icon: image,
                    title: text,
                    zIndex: 999,
                    icon: image
                });//добавление маркера

                $scope.markers.push(marker);
    };

    $scope.clearMarkers = function() {
        $scope.markers.forEach(function(item, i, arr) {
                item.setMap(null);
            });     
        $scope.markers.length = 0;
    }; 

    

    $scope.formatDate = function(dt) {
        Date.prototype.addHours= function(h){
            this.setHours(this.getHours()+h);
            return this;
        };
        //1) вычесть 621355968000000000L
        //2) Поделить на 10000 -- получить миллисекунды
        //3) Вызвать конструктор для Date(миллисекунды) и передать туда полученные миллисекунды в п.2
        var d = dt - 621355968000000000;
        d = d / 10000;        
 
       
        var tickDate = new Date(d);
        //var offset = tickDate.getTimezoneOffset();
        //var hOffset = offset/60;
        //tickDate = tickDate.addHours(hOffset);
        
        return tickDate;
    };


    // код для всплывающего окна
    $scope.visiblePopup = false;

    $scope.popupTitle = '';

    $scope.popupOptions = {
        width: 400,
        height: 150,
        contentTemplate: "info",
        showTitle: true,
        title: "Информация",    
        dragEnabled: false,
        closeOnOutsideClick: true,
        bindingOptions: {
            visible: "visiblePopup",
        }
    };
    

}]);