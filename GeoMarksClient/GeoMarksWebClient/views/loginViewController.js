myApp = angular.module('myApp')
myApp.controller('loginViewController', ['$scope', '$http', '$location', 'authorizationService', function ($scope, $http, $location, authorizationService) {

 	if (authorizationService.isAuthorized() == true) {
        $location.path('/map');
    }

	$scope.login = function() {
		authorizationService.login($scope.username, $scope.password).then( function() {
			if (authorizationService.isAuthorized() == true) {
				var user = authorizationService.getAuthorizedUser();


				if (authorizationService.checkStateRights('map') == false) {
				        $location.path('/denied');
				};

				$location.path('/map');
			}
		});
	}
}]);