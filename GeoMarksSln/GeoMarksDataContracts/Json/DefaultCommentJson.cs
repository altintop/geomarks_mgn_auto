﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SmartDocuments.GeoMarksDataContracts.Json
{
    [DataContract]
    public class DefaultCommentJson
    {
        [DataMember(Order = 1)]
        public virtual int Id { get; set; }
        [DataMember(Order = 2)]
        public virtual string Text { get; set; }
        [DataMember(Order = 3)]
        public virtual string Alias { get; set; }
    }
}
