﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartDocuments.GeoMarksUnitTests.GeoMarksSoapReference;

namespace SmartDocuments.GeoMarksUnitTests
{
    [TestClass]
    public class GeoMarksRestServiceTest
    {
        [TestMethod]
        public void TestInsertNewUser()
        {
            //var client = new GeoMarksSoapSvcClient();
            //var newUser = new User()
            //{
            //    Email = "test@wodo.ru",
            //    FirstName = "Руфатъ",
            //    LastName = "Егоров",
            //    Password = "111111",
            //    Patronymic = "Петрович",
            //    PhoneNumber = "9171234567"
            //};
            //client.InsertNewUser(newUser, RoleEnum.Commissar);

            var client = new GeoMarksSoapSvcClient();
        }

        [TestMethod]
        public void TestCalcDistances()
        {
            var lat1 = 54.7303429;
            var lon1 = 55.9786909;
            var lat2 = 54.73842;
            var lon2 = 55.981916;
            var p = 0.017453292519943295;    // Math.PI / 180
            var a = 0.5 - Math.Cos((lat2 - lat1) * p) / 2 +
                    Math.Cos(lat1 * p) * Math.Cos(lat2 * p) *
                    (1 - Math.Cos((lon2 - lon1) * p)) / 2;
            var dist = (double)(12742 * Math.Asin(Math.Sqrt(a))) * 1000; // 2 * R; R = 6371 km
            dist = dist + 1;
        }
    }
}
