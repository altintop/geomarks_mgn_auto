package ru.smart_documents.geoapp;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nail on 23.05.2016.
 */
public class GeoAppApplication extends Application {
    // Флаг, отвечающий за автовход
    private boolean isNeedAutoLogin = true;
    // Список идентификаторов меток текущего пользователя (аваркома)
    private List<Integer> userMarkIds = new ArrayList<>();

    public boolean getIsNeedAutoLogin() {
        return isNeedAutoLogin;
    }

    public void setIsNeedAutoLogin(boolean boolVar) {
        this.isNeedAutoLogin = boolVar;
    }

    public List<Integer> getUserMarkIds() {
        return userMarkIds;
    }

    public void addUserMarkIds(int markId) {
        userMarkIds.add(markId);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
