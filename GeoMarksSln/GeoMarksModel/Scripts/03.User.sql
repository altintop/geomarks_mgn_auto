﻿-- Table: user_gm

-- DROP TABLE user_gm;

CREATE TABLE user_gm
(
  id serial NOT NULL, -- Идентификатор пользователя
  password character varying(200) NOT NULL, -- Пароль пользователя
  firstname character varying(100) NOT NULL, -- Имя пользователя
  lastname character varying(100) NOT NULL, -- Фамилия пользователя
  roleid integer NOT NULL, -- Внешний ключ на таблицу ролей пользователей
  phonenumber character varying(10) NOT NULL, -- Номер телефона (10 цифр)
  email character varying(100) NOT NULL, -- Адрес электронная почта
  CONSTRAINT pk_user_id PRIMARY KEY (id),
  CONSTRAINT fk_user_to_user_role FOREIGN KEY (roleid)
      REFERENCES user_role (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_gm
  OWNER TO postgres;
COMMENT ON TABLE user_gm
  IS 'Таблица пользователей';
COMMENT ON COLUMN user_gm.id IS 'Идентификатор пользователя';
COMMENT ON COLUMN user_gm.password IS 'Пароль пользователя';
COMMENT ON COLUMN user_gm.firstname IS 'Имя пользователя';
COMMENT ON COLUMN user_gm.lastname IS 'Фамилия пользователя';
COMMENT ON COLUMN user_gm.roleid IS 'Внешний ключ на таблицу ролей пользователей';
COMMENT ON COLUMN user_gm.phonenumber IS 'Номер телефона (10 цифр)';
COMMENT ON COLUMN user_gm.email IS 'Адрес электронная почта';


-- Index: fki_user_to_user_role

-- DROP INDEX fki_user_to_user_role;

CREATE INDEX fki_user_to_user_role
  ON user_gm
  USING btree
  (roleid);

