myApp = angular.module('myApp')
myApp.controller('employeesViewController', ['$scope', 'employeesFactory', '$http', '$location', 'authorizationService', function ($scope, employeesFactory, $http, $location, authorizationService) {
	
    if (authorizationService.checkStateRights('employee') == false) {
        $location.path('/denied');
    }

    Globalize.culture(navigator.language || navigator.browserLanguage);

    
    var grid = null;    
    
    $scope.employees = [];

    var roles = [{
            "ID": 1,
            "Name": "Аварком"         
        }, {
            "ID": 2,
            "Name": "Менеджер"            
        }, {
            "ID": 3,
            "Name": "Информатор"            
        },
        {
            "ID": 4,
            "Name": "Админ"            
        }];


    // Описание грида
    
    $scope.gridOptions = {
            editing: {
                editEnabled: true,
                editMode: 'row',
                //insertEnabled: true,
                removeEnabled: true
            },

            allowColumnResizing: true,
            allowColumnReordering: true,
            filterRow: {
                visible: true
            },
            groupPanel: {
                visible: true
            },
            "export": {
                enabled: true,
                fileName: "Employees",
                allowExportSelectedData: true
            },
            bindingOptions: {
                dataSource: 'employees',            
            },

            columns: [
                {
                    dataField: "Id",
                    caption: "№",
                    width: 40,
                    allowEditing: false 
                },
                {
                    dataField: "FirstName",
                    caption: "Имя",
                    //width: 150                    
                },
                {
                    dataField: "LastName",
                    caption: "Фамилия",
                    //width: 150                    
                },
                {
                    dataField: "Patronymic",
                    caption: "Отчество",
                    //width: 150                    
                },
                {
                    dataField: "PhoneNumber",
                    caption: "Номер телефона",
                    //width: 200                    
                },   
                {
                    dataField: "Email",                    
                    //width: 150,
                    caption: "Электронная почта"                    
                },
                {
                    dataField: "Password",                    
                    //width: 100,
                    caption: "Пароль"                    
                },
                 {
                    dataField: "RoleId",                    
                    //width: 100,
                    caption: "Роль",
                    lookup: {
                        dataSource: roles,
                        displayExpr: "Name",
                        valueExpr: "ID"
                    }                    
                }, 
    
                
            ],
            paging: { pageSize: 20 },
            contentReadyAction: function(e) {
                                   grid = e.component;
                                },
            onRowUpdated: function(e) {
                
                if (e != null) {
                    if (e.key != null) {
                        employeesFactory.updateEmployee(e.key);
                    }                                
                }
            },
            onRowRemoving: function(e) {
                employeesFactory.deleteEmployee(e.key);    
            },
            /*
            onRowInserted: function(e) {
                if (e != null) {
                    if(e.key != null) {
                         employeesFactory.insertEmployee(e.key);
                    }                                
                }
            },
            onRowInserted: function(e) {
                logEvent("RowInserted");
            },
            */
            /*
            onRowInserting: function(e) {             
                if (e != null) {
                    var employee = {
                        "Id": "0",
                        "FirstName": e.data.FirstName,
                        "LastName": e.data.LastName,
                        "Patronymic": e.data.Patronymic,
                        "PhoneNumber": e.data.PhoneNumber,
                        "Email": e.data.Email,
                        "Password": e.data.Password,
                        "RoleId": 1
                    };

                    //$scope.employees.push(employee)
                    
                    employeesFactory.insertEmployee(employee).then(function() {
                        e.data.Id = employee.Id;
                    });                    
                }             
            },
            */
        };

    // Получение всех пользователей
    employeesFactory.getAllEmployees().then(function(response){
        $scope.employees = response;
        if (grid) grid.refresh();       
    });

    $scope.addEmployee = function() {
        var employee = {
            "Id": "0",
            "FirstName": "",
            "LastName": "",
            "Patronymic": "",
            "PhoneNumber": "",
            "Email": "",
            "Password": "123",
            "RoleId": 1
        };

        employeesFactory.insertEmployee(employee).then(function() {                        
            $scope.employees.push(employee);
        });        
    }
}]);