﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksModel.Mappers
{
    public class MarkMap : ClassMap<Mark> {
        
        public MarkMap() {
            Table("mark");

            Id(x => x.Id).GeneratedBy.Identity().Column("id");

            Map(x => x.Latitude).Column("latitude").Not.Nullable();
            Map(x => x.Longitude).Column("longitude").Not.Nullable();
            Map(x => x.CreateTime).Column("createtime").Not.Nullable();
            Map(x => x.LastUpdatedTime).Column("lastupdatedtime");
            Map(x => x.Comment).Column("comment");
            Map(x => x.Address).Column("address");

            References(x => x.CreateUser).Not.Nullable().Not.LazyLoad();
            References(x => x.AssignedUser).Not.LazyLoad();
            References(x => x.State).Not.Nullable().Not.LazyLoad();
        }
    }
}
