package ru.smart_documents.geoapp;

import ru.smart_documents.geoapp.dtos.UserInfo;

/**
 * Created by Galeev.NR on 14.04.2016.
 */

public class MyProperties {
    private static MyProperties mInstance= null;

    public UserInfo userInfo;

    protected MyProperties(){
        userInfo = new UserInfo();
    }

    public static synchronized MyProperties getInstance(){
        if(null == mInstance){
            mInstance = new MyProperties();
        }
        return mInstance;
    }
}
