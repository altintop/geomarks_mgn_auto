-- Table: "user_role"

-- DROP TABLE user_role;

CREATE TABLE user_role
(
  id integer NOT NULL, -- ������������� ����
  alias character varying(100) NOT NULL, -- ������� �������� ����
  name character varying(100) NOT NULL, -- �������� ���� �� ������� �����
  CONSTRAINT pk_user_role_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_role
  OWNER TO postgres;
COMMENT ON TABLE user_role
  IS '�������-���������� ����� �������������';
COMMENT ON COLUMN user_role.id IS '������������� ����';
COMMENT ON COLUMN user_role.alias IS '������� �������� ����';
COMMENT ON COLUMN user_role.name IS '�������� ���� �� ������� �����';

