﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SmartDocuments.GeoMarksDataContracts.DTOs
{
    [DataContract(IsReference = true)]
    public class UserCoordinates
    {
        [DataMember(Order = 1)]
        public virtual int Id { get; protected set; }
        [DataMember(Order = 2)]
        public virtual double Longitude { get; set; }
        [DataMember(Order = 3)]
        public virtual double Latitude { get; set; }
        [DataMember(Order = 4)]
        public virtual DateTime UpdateTime { get; set; }
        [DataMember(Order = 5)]
        public virtual User User { get; set; }
    }
}
