﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartDocuments.GeoMarksDataContracts;
using SmartDocuments.GeoMarksDataContracts.DTOs;
using SmartDocuments.GeoMarksUnitTests.GeoMarksSoapReference;

namespace GeoMarksUnitTests
{
    [TestClass]
    public class GeoMarksSoapServiceTest
    {
        [TestMethod]
        public void TestInsertNewUser()
        {
            //var client = new GeoMarksSoapSvcClient();
            //var newUser = new User()
            //{
            //    Email = "test@wodo.ru",
            //    FirstName = "Руфатъ",
            //    LastName = "Егоров",
            //    Password = "111111",
            //    Patronymic = "Петрович",
            //    PhoneNumber = "9171234567"
            //};
            //client.InsertNewUser(newUser, RoleEnum.Commissar);

            var client = new GeoMarksSoapSvcClient();
            var newUser = new User()
            {
                Email = "rust@bolgar.com",
                FirstName = "Рустем",
                LastName = "Гайзуллин",
                Password = "112335567778",
                Patronymic = "Радикович",
                PhoneNumber = "9171234777"
            };
            client.InsertNewUser(newUser, RoleEnum.Informant);
        }
    }
}
