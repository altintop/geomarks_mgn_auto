﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksDataContracts.Json
{
    [DataContract]
    public class UserCordinatesJson
    {
        [DataMember(Order = 1)]
        public virtual int Id { get; set; }
        [DataMember(Order = 2)]
        public virtual double Longitude { get; set; }
        [DataMember(Order = 3)]
        public virtual double Latitude { get; set; }
        [DataMember(Order = 4)]
        public virtual long UpdateTime { get; set; }
        [DataMember(Order = 5)]
        public virtual int UserId { get; set; }
    }
}
