package ru.smart_documents.geoapp.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import ru.smart_documents.geoapp.GeoAppApplication;
import ru.smart_documents.geoapp.GeoMarksRestClient;
import ru.smart_documents.geoapp.MarkStateEnum;
import ru.smart_documents.geoapp.MyProperties;
import ru.smart_documents.geoapp.R;
import ru.smart_documents.geoapp.UIUpdater;
import ru.smart_documents.geoapp.dtos.MarkInfo;
import ru.smart_documents.geoapp.dtos.UserInfo;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class CommissarControlActivity extends AppCompatActivity implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener {

    private UserInfo mUserInfo;
    private View mProgressView;
    private GetMarksForUserTask mGetMarksTask = null;
    private SendCurCoordsToSvcTask mSendCurCoordsTask = null;
    private UpdateMarkForUserTask mUpdateMarkTask = null;
    private List<MarkInfo> mMarks = new ArrayList<MarkInfo>();
    private UIUpdater mUIUpdater;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 5000; // 5 sec
    private static int FASTEST_INTERVAL = 2000; // 2 sec
    private static int DISPLACEMENT = 5; // 5 meters

    private static final long TICKS_AT_EPOCH = 621355968000000000L;
    private static final long TICKS_PER_MILLISECOND = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commissar_control);

        // ставим ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.commiss_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mProgressView = findViewById(R.id.commiss_control_progress);
        mUserInfo = MyProperties.getInstance().userInfo;
        mUIUpdater = new UIUpdater(new Runnable() {
            @Override
            public void run() {
                getMarksForUser();
            }
        });

        // Начинаем обновлять таблицу заметок
        mUIUpdater.startUpdates();

        // получаем mLocationManager
        //mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        // We need to check availability of play services
        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // выводим ФИО юзера в шапку
        mUserInfo = MyProperties.getInstance().userInfo;
        getSupportActionBar().setSubtitle(mUserInfo.getFirstName() + " " + mUserInfo.getLastName());
        // Проверяем разрешения на геолокацию
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            checkPlayServices();
            // Resuming the periodic location updates
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Приложению необходимы разрешения для доступа к данным о местоположении", Toast.LENGTH_LONG).show();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int REQUEST_CODE_ASK_PERMISSIONS = 123;
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        // Заканчиваем обновлять таблицу заметок
        mUIUpdater.stopUpdates();
        stopLocationUpdates();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Метод вызывает task для получения списка заметок пользователя
     */
    private void getMarksForUser() {
        if (mGetMarksTask != null)
            return;
        showProgress(true);
        mGetMarksTask = new GetMarksForUserTask();
        mGetMarksTask.execute((Void) null);
    }

    /**
     * Метод вызывает task для изменения заметки пользователя
     */
    private void updateMarkForUser(int markId, int assignedUserId, int stateId, int rowId) {
        if (mUpdateMarkTask != null)
            return;
        showProgress(true);
        mUpdateMarkTask = new UpdateMarkForUserTask(markId, assignedUserId, stateId, rowId);
        mUpdateMarkTask.execute((Void) null);
    }

    private void showStatesAfterInHandState(final int markId, final int assignedUserId, final int rowId) {
        final LinkedHashMap<CharSequence,Integer> statuses = new LinkedHashMap<CharSequence,Integer>();
        statuses.put(getString(R.string.accompany), MarkStateEnum.Accompanied.getValue());
        statuses.put(getString(R.string.refused), MarkStateEnum.Refused.getValue());
        statuses.put(getString(R.string.dispute), MarkStateEnum.Dispute.getValue());
        statuses.put(getString(R.string.hurt), MarkStateEnum.Hurt.getValue());
        statuses.put(getString(R.string.disappeared), MarkStateEnum.Disappeared.getValue());
        statuses.put(getString(R.string.mistaken), MarkStateEnum.Mistaken.getValue());
        statuses.put(getString(R.string.late), MarkStateEnum.Late.getValue());
        final CharSequence[] items = Arrays.copyOf(statuses.keySet().toArray(), statuses.size(), CharSequence[].class);
        new AlertDialog.Builder(this)
                .setTitle("Выберите статус")
                .setSingleChoiceItems(items, 0, null)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        // Do something useful withe the position of the selected radio button
                        int markStateId = statuses.get(items[selectedPosition]);
                        updateMarkForUser(markId, assignedUserId, markStateId, rowId);
                    }
                })
                .setNegativeButton("Отмена", null)
                .show();
    }

    private void sendLocation(Location location) {
        if (location == null || mUserInfo == null)
            return;
        if (mSendCurCoordsTask != null)
            return;
        mSendCurCoordsTask = new SendCurCoordsToSvcTask(mUserInfo.getId(), location.getLatitude(), location.getLongitude(), location.getTime() * TICKS_PER_MILLISECOND + TICKS_AT_EPOCH);
        mSendCurCoordsTask.execute((Void) null);
    }

    private String dateToString(Date date) {
        String DATE_FORMAT_NOW = "dd.MM.yyyy HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        TimeZone tz = TimeZone.getDefault();
        sdf.setTimeZone(tz);
        return sdf.format(date);
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     * */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     * */
    protected void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    /**
     * Shows the progress UI
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        sendLocation(location);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * Таск получает список меток для текущего пользователя через REST-сервис
     */
    public class GetMarksForUserTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            final boolean[] result = {false};
            GeoMarksRestClient client = new GeoMarksRestClient();
            RequestParams rp = new RequestParams();
            rp.put("userId", mUserInfo.getId());
            client.get("GetMarksForUser", rp, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    try {
                        mMarks = new ArrayList<MarkInfo>();
                        if (response.length() > 0) {
                            boolean isCameNewMarks = false;
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject markObject = (JSONObject) response.get(i);
                                MarkInfo mi = new MarkInfo();
                                mi.setId(markObject.getInt("Id"));
                                mi.setLatitude(markObject.getDouble("Latitude"));
                                mi.setLongitude(markObject.getDouble("Longitude"));
                                mi.setStateId(markObject.getInt("StateId"));
                                mi.setComment(markObject.getString("Comment"));
                                mi.setAddress(markObject.getString("Address"));
                                mi.setAssignedUserId(markObject.getInt("AssignedUserId"));
                                mi.setCreateUserId(markObject.getInt("CreateUserId"));
                                Date createDt = new Date((markObject.getLong("CreateTime") - TICKS_AT_EPOCH) / TICKS_PER_MILLISECOND);
                                mi.setCreateTime(createDt);
                                mMarks.add(mi);
                                // добавляем id меток, если пришли новые метки
                                GeoAppApplication geoApp = ((GeoAppApplication) CommissarControlActivity.this.getApplication());
                                if(!geoApp.getUserMarkIds().contains(mi.getId())) {
                                    geoApp.addUserMarkIds(mi.getId());
                                    isCameNewMarks = true;
                                }
                            }
                            if(isCameNewMarks) {
                                NotificationCompat.Builder mb = new NotificationCompat.Builder(CommissarControlActivity.this);
                                mb.setContentTitle("Помощь при ДТП.");
                                mb.setContentText("Список меток обновлён!");
                                mb.setSmallIcon(R.drawable.notification_icon);
                                // Sets an ID for the notification
                                int mNotificationId = 001;
                                // Gets an instance of the NotificationManager service
                                NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                // Builds the notification and issues it.
                                mNotifyMgr.notify(mNotificationId, mb.build());
                                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.notification);
                                mp.start();
                            }
                        }
                        result[0] = true;
                    } catch (JSONException exc) {
                        result[0] = false;
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (statusCode == 503) {
                        CommissarControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else if (statusCode == 400) {
                        CommissarControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    // When Http response code other than 400, 503
                    else {
                        CommissarControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    result[0] = false;
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (statusCode == 503) {
                        CommissarControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else if (statusCode == 400) {
                        CommissarControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    // When Http response code other than 400, 503
                    else {
                        CommissarControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    result[0] = false;
                }
            });
            return result[0];
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mGetMarksTask = null;
            showProgress(false);

            if (success) {
                TableLayout markTable = (TableLayout) findViewById(R.id.displayMarksContent);
                markTable.removeAllViewsInLayout();
                if (mMarks.size() > 0) {
                    for (int j = 0; j < mMarks.size(); j++) {
                        // Формируем таблицу отметок
                        final int finalJ = j;
                        final MarkInfo mi = mMarks.get(j);
                        TableRow row = new TableRow(CommissarControlActivity.this);
                        TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
                        row.setLayoutParams(lp);
                        TypedValue outValue = new TypedValue();

                        TextView dateTv = new TextView(CommissarControlActivity.this);
                        getResources().getValue(R.dimen.date_width_percent, outValue, true);
                        lp = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, outValue.getFloat());
                        dateTv.setLayoutParams(lp);
                        dateTv.setPadding(5, 2, 5, 2);
                        dateTv.setText(dateToString(mi.getCreateTime()));
                        if (Build.VERSION.SDK_INT < 23) { dateTv.setTextAppearance(CommissarControlActivity.this, android.R.style.TextAppearance_Small);}
                        else { dateTv.setTextAppearance(android.R.style.TextAppearance_Small); }

                        TextView addressTv = new TextView(CommissarControlActivity.this);
                        getResources().getValue(R.dimen.address_width_percent, outValue, true);
                        lp = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, outValue.getFloat());
                        addressTv.setLayoutParams(lp);
                        addressTv.setPadding(5, 2, 5, 2);
                        addressTv.setText(mi.getAddress());
                        /*try {
                            addressTv.setText(getAddressLine(mi.getLatitude(), mi.getLongitude()));
                        } catch (IOException e) {
                            e.printStackTrace();
                        }*/
                        if (Build.VERSION.SDK_INT < 23) { addressTv.setTextAppearance(CommissarControlActivity.this, android.R.style.TextAppearance_Small);}
                        else { addressTv.setTextAppearance(android.R.style.TextAppearance_Small); }

                        TextView commentTv = new TextView(CommissarControlActivity.this);
                        getResources().getValue(R.dimen.comment_width_percent, outValue, true);
                        lp = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, outValue.getFloat());
                        commentTv.setLayoutParams(lp);
                        commentTv.setPadding(5, 2, 5, 2);
                        commentTv.setText(mi.getComment());
                        if (Build.VERSION.SDK_INT < 23) { commentTv.setTextAppearance(CommissarControlActivity.this, android.R.style.TextAppearance_Small);}
                        else { commentTv.setTextAppearance(android.R.style.TextAppearance_Small); }

                        Button btn = new Button(CommissarControlActivity.this);
                        //btn.setTextAppearance(CommissarControlActivity.this, android.R.style.TextAppearance_DeviceDefault_Small);
                        btn.setTransformationMethod(null);
                        btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (mi.getStateId() == MarkStateEnum.Assigned.getValue()) {
                                    updateMarkForUser(mi.getId(), mi.getAssignedUserId(), MarkStateEnum.InHand.getValue(), finalJ);
                                } else if (mi.getStateId() == MarkStateEnum.Accompanied.getValue()) {
                                    updateMarkForUser(mi.getId(), mi.getAssignedUserId(), MarkStateEnum.Completed.getValue(), finalJ);
                                } else if(mi.getStateId() == MarkStateEnum.InHand.getValue()) {
                                    showStatesAfterInHandState(mi.getId(), mi.getAssignedUserId(), finalJ);
                                }
                            }
                        });
                        getResources().getValue(R.dimen.btn_width_percent, outValue, true);
                        lp = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT, outValue.getFloat());
                        btn.setLayoutParams(lp);
                        setButtonTextByMarkState(mi, btn);
                        if (Build.VERSION.SDK_INT < 23) { btn.setTextAppearance(CommissarControlActivity.this, android.R.style.TextAppearance_Small);}
                        else { btn.setTextAppearance(android.R.style.TextAppearance_Small); }

                        row.addView(dateTv);
                        row.addView(addressTv);
                        row.addView(commentTv);
                        row.addView(btn);
                        markTable.addView(row, j);
                    }
                }
            } else {
                Toast.makeText(getApplicationContext(), "Метки не получены!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mGetMarksTask = null;
            showProgress(false);
        }
    }

    /**
     * Таск отсылает координаты на сервер через REST-сервис
     */
    public class SendCurCoordsToSvcTask extends AsyncTask<Void, Void, Boolean> {

        private final double mLatitude;
        private final double mLongitude;
        private final long mDateTime;
        private final int mUserId;

        SendCurCoordsToSvcTask(int userId, double latitude, double longitude, long dateTime) {
            mLatitude = latitude;
            mLongitude = longitude;
            mDateTime = dateTime;
            mUserId = userId;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final boolean[] result = {false};
            GeoMarksRestClient client = new GeoMarksRestClient();
            JSONObject jso = new JSONObject();
            try {
                jso.put("Latitude", mLatitude);
                jso.put("Longitude", mLongitude);
                jso.put("UpdateTime", mDateTime);
                jso.put("UserId", mUserId);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StringEntity entity = null;
            try { entity = new StringEntity(jso.toString()); } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            if(entity != null) {
                client.post(CommissarControlActivity.this, "usrcoords/", entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        result[0] = true;
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        if (statusCode == 503) {
                            CommissarControlActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else if (statusCode == 400) {
                            CommissarControlActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        // When Http response code other than 400, 503
                        else {
                            CommissarControlActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        result[0] = false;
                    }
                });
            }
            return result[0];
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mSendCurCoordsTask = null;

            /*if (success) {
                Toast.makeText(getApplicationContext(), "Координаты отправлены!", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Координаты не отправлены!", Toast.LENGTH_SHORT).show();
            }*/
        }

        @Override
        protected void onCancelled() {
            mSendCurCoordsTask = null;
        }
    }

    /**
     * Таск обновляет метку для текущего пользователя через REST-сервис
     */
    public class UpdateMarkForUserTask extends AsyncTask<Void, Void, Boolean> {

        private final MarkInfo mMark;
        private int mRowIndex;

        UpdateMarkForUserTask(int markId, int assignedUserId, int stateId, int rowIndex) {
            mMark = new MarkInfo();
            mMark.setId(markId);
            mMark.setAssignedUserId(assignedUserId);
            mMark.setStateId(stateId);
            mRowIndex = rowIndex;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final boolean[] result = {false};
            GeoMarksRestClient client = new GeoMarksRestClient();
            //JSONArray jsArray = new JSONArray();
            JSONObject jso = new JSONObject();
            try {
                jso.put("Id", mMark.getId());
                jso.put("AssignedUserId", mMark.getAssignedUserId());
                jso.put("StateId", mMark.getStateId());
                //jsArray.put(jso);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            StringEntity entity = null;
            try { entity = new StringEntity(jso.toString()); } catch (UnsupportedEncodingException e) { e.printStackTrace(); }
            if(entity != null) {
                client.post(CommissarControlActivity.this, "marker/", entity, "application/json", new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        result[0] = true;
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        if (statusCode == 503) {
                            CommissarControlActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else if (statusCode == 400) {
                            CommissarControlActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                        // When Http response code other than 400, 503
                        else {
                            CommissarControlActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        result[0] = false;
                    }
                });
            }
            return result[0];
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mUpdateMarkTask = null;
            showProgress(false);

            if (success) {
                // Если переводим метку в статус Завершено, то нужно обновить список меток, чтобы она исчезла
                if(mMark.getStateId() != MarkStateEnum.Accompanied.getValue() && mMark.getStateId() != MarkStateEnum.InHand.getValue()){
                    getMarksForUser();
                }
                else {
                    //getMarksForUser();

                    TableLayout markTable = (TableLayout) findViewById(R.id.displayMarksContent);
                    TableRow tr = (TableRow) markTable.getChildAt(mRowIndex);
                    Button btn = (Button) tr.getChildAt(3);
                    setButtonTextByMarkState(mMark, btn);
                    btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mMark.getStateId() == MarkStateEnum.Assigned.getValue()) {
                                updateMarkForUser(mMark.getId(), mMark.getAssignedUserId(), MarkStateEnum.Accompanied.getValue(), mRowIndex);
                            } else if (mMark.getStateId() == MarkStateEnum.Accompanied.getValue()) {
                                updateMarkForUser(mMark.getId(), mMark.getAssignedUserId(), MarkStateEnum.Completed.getValue(), mRowIndex);
                            } else if(mMark.getStateId() == MarkStateEnum.InHand.getValue()) {
                                showStatesAfterInHandState(mMark.getId(), mMark.getAssignedUserId(), mRowIndex);
                            }
                        }
                    });
                }
            } else {
                Toast.makeText(getApplicationContext(), "Метка не обновлена!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mUpdateMarkTask = null;
            showProgress(false);
        }
    }

    private void setButtonTextByMarkState(MarkInfo mi, Button btn) {
        if(mi.getStateId() == MarkStateEnum.Assigned.getValue()) {
            btn.setText(getString(R.string.take_to_hand));
        }
        else {
            if (mi.getStateId() == MarkStateEnum.InHand.getValue()) {
                btn.setText(getString(R.string.change_state));
            }
            else {
                if (mi.getStateId() == MarkStateEnum.Accompanied.getValue()) {
                    btn.setText(getString(R.string.complete));
                }
                else {
                    btn.setText("---");
                }
            }
        }
    }
}
