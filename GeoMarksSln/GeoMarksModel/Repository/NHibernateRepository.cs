﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Conventions.Helpers;
using NHibernate.Tool.hbm2ddl;

namespace SmartDocuments.GeoMarksModel.Repository
{
    public class NHibernateRepository<T> : IRepository<T> where T : class
    {
        protected FluentConfiguration config;
        protected ISessionFactory sessionFactory;

        public NHibernateRepository()
        {
            //var dbConfig = PostgreSQLConfiguration.PostgreSQL82.ConnectionString(Properties.Settings.Default.ConnString);
            var connectionStrings = ConfigurationManager.ConnectionStrings;
            string pgSqlConnString = "";
            foreach (ConnectionStringSettings cs in connectionStrings)
            {
                if (cs.Name.Equals("postgreSqlConnString"))
                    pgSqlConnString = cs.ConnectionString;
            }
            var dbConfig = PostgreSQLConfiguration.PostgreSQL82.ConnectionString(pgSqlConnString);
            config = Fluently.Configure().Database(dbConfig).Mappings(m => m.FluentMappings.AddFromAssemblyOf<NHibernateRepository<T>>()).ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(false, true));
            //config = Fluently.Configure().Database(dbConfig).Mappings(m => m.FluentMappings.AddFromAssemblyOf<NHibernateRepository<T>>()).ExposeConfiguration(cfg => new SchemaExport(cfg).Execute(true, true, false));
            sessionFactory = config.BuildSessionFactory();
        }

        public void Delete(T value)
        {
            using (var session = sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Delete(value);
                transaction.Commit();
            }
        }

        public T Get(object id)
        {
            T result = null;
            using (var session = sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                result = session.Get<T>(id);
                transaction.Commit();
            }
            return result;
        }

        public IList<T> GetAll()
        {
            IList<T> result = null;
            using (var session = sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                result = session.Query<T>().ToList<T>();
                transaction.Commit();
            }
            return result;
        }

        public void Save(T value)
        {
            using (var session = sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Save(value);
                transaction.Commit();
            }
        }

        public void Update(T value)
        {
            using (var session = sessionFactory.OpenSession())
            using (var transaction = session.BeginTransaction())
            {
                session.Update(value);
                transaction.Commit();
            }
        }

        public int  Count()
        {
            using (var session = sessionFactory.OpenSession())            
            {
                return session.Query<T>().Count();                
            }            
        }
    }
}
