﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using SmartDocuments.GeoMarksDataContracts.DTOs;
using SmartDocuments.GeoMarksDataContracts.Json;
using SmartDocuments.GeoMarksDataContracts;
using System;

namespace SmartDocuments.GeoMarksRestService
{
    [ServiceContract]
    public interface IGeoMarksRestSvc
    {

        [OperationContract]
        [WebGet(UriTemplate = "/ValidateUser/?phoneNumber={phoneNumber}&password={password}", ResponseFormat = WebMessageFormat.Json)]
        UserJson ValidateUser(string phoneNumber, string password);

        [OperationContract]
        [WebGet(UriTemplate = "/GetMarksForUser/?userId={userId}", ResponseFormat = WebMessageFormat.Json)]
        List<MarkJson> GetMarksForUser(int userId);

        [OperationContract]
        [WebGet(UriTemplate = "/CreateMark/?userId={userId}&latitude={latitude}&longitude={longitude}&comment={comment}&address={address}", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        bool CreateMark(int userId, double latitude, double longitude, string comment, string address);

        [OperationContract]
        [WebGet(UriTemplate = "/marker/?period={period}&beginDate={beginDate}&endDate={endDate}", ResponseFormat = WebMessageFormat.Json)]
        List<MarkJson> GetMarks(MarksPeriodEnum period, string beginDate, string endDate);

        [OperationContract]
        [WebGet(UriTemplate = "/marker/count/", ResponseFormat = WebMessageFormat.Json)]
        int GetMarksCount();

        [OperationContract]
        [WebGet(UriTemplate = "/avarcom/", ResponseFormat = WebMessageFormat.Json)]
        List<UserJson> GetAvarcoms();

        [OperationContract]
        [WebGet(UriTemplate = "/taxidriver/", ResponseFormat = WebMessageFormat.Json)]
        List<UserJson> GetTaxiDrivers();

        [OperationContract]
        [WebGet(UriTemplate = "/informants/", ResponseFormat = WebMessageFormat.Json)]
        List<UserJson> GetInformants();

        [OperationContract]
        [WebGet(UriTemplate = "/defcomments/", ResponseFormat = WebMessageFormat.Json)]
        List<DefaultCommentJson> GetDefaultComments();

        //[OperationContract]
        //[WebInvoke(Method = "POST", UriTemplate = "/marker/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        //void UpdateMarks(List<MarkJson> marks);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/marker/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateMark(MarkJson mark);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/delete-marker/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void DeleteMarker(MarkJson mark);

        [OperationContract]
        [WebGet(UriTemplate = "/GetUserCoords/?userId={userId}", ResponseFormat = WebMessageFormat.Json)]
        UserCordinatesJson GetUserCoords(int userId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/usrcoords/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void CreateOrUpdateUserCoords(UserCordinatesJson usrCoords);

        [OperationContract]
        [WebGet(UriTemplate = "/employee/", ResponseFormat = WebMessageFormat.Json)]
        List<UserJson> GetEmploees();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/update-employee/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void UpdateEmployee(UserJson user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/employee/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        UserJson CreateNewEmployee(UserJson user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/delete-employee/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void DeleteEmployee(UserJson user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/coords/avarcom/", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<UserCordinatesJson> GetAvarcomCoords(List<int> userIds);
    }
}
