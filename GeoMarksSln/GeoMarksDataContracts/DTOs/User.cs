﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SmartDocuments.GeoMarksDataContracts.DTOs
{
    [DataContract(IsReference = true)]
    public class User
    {
        public User()
        {
            CreatedMarks = new List<Mark>();
            AssignedMarks = new List<Mark>();
        }
        [DataMember(Order = 1)]
        public virtual int Id { get; protected set; }
        [DataMember(Order = 2)]
        public virtual string PhoneNumber { get; set; }
        [DataMember(Order = 3)]
        public virtual string Email { get; set; }
        [DataMember(Order = 4)]
        public virtual string FirstName { get; set; }
        [DataMember(Order = 5)]
        public virtual string Password { get; set; }
        [DataMember(Order = 6)]
        public virtual string LastName { get; set; }
        [DataMember(Order = 7)]
        public virtual string Patronymic { get; set; }
        [DataMember(Order = 8)]
        public virtual UserRole Role { get; set; }
        [DataMember(Order = 9)]
        public virtual IList<Mark> CreatedMarks { get; set; }
        [DataMember(Order = 10)]
        public virtual IList<Mark> AssignedMarks { get; set; }
        [DataMember(Order = 11)]
        public virtual UserCoordinates CurrentCoordinates { get; set; }
    }
}
