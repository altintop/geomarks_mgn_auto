'use strict';

var myApp = angular.module('myApp');
myApp.factory('employeesFactory', ['$http', '$q', 'appSettings', function($http, $q, appSettings) {

    //var urlBase = 'http://localhost:49545/GeoMarksRestSvc.svc/';
    var urlBase = appSettings.baseUrl;    
    var urlAvarcoms = urlBase + 'avarcom/';
    //var urlTaxiDrivers = urlBase + 'taxidriver/';
    var urlInformant = urlBase + 'informants/';
    var urlEmployees = urlBase + 'employee/';
    var urlNewEmployee = urlBase + 'new-employee/';
    var urlUpdateEmployee = urlBase + 'update-employee/';
    var urlDeleteEmployee = urlBase + 'delete-employee/';
    var urlAvarcomCoords = urlBase + 'coords/avarcom/';
    

    var factory = {}; 

    factory.getAllTaxiDrivers = function() {        
        return $http.get(urlInformant)
        .then(function(response) {
                    if (typeof response.data === 'object') {
                            return response.data;
                    } else 
                    {
                    // invalid response
                        return $q.reject(response.data);
                    }
                }, function(response) {
                    // something went wrong
                    return $q.reject(response.data);
                });                
        };

    factory.getAllAvarcoms = function() {
        return $http.get(urlAvarcoms)
        .then(function(response) {
                    if (typeof response.data === 'object') {
                            return response.data;
                    } else 
                    {
                    // invalid response
                        return $q.reject(response.data);
                    }
                }, function(response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
    };

    factory.getAllEmployees = function() {
      return $http.get(urlEmployees)
                .then(function(response) {
                    if (typeof response.data === 'object') {
                            var arr = response.data;

                            // удаляем 0 элемент массива - он служебный
                            for(var i = arr.length - 1; i >= 0; i--) {
                                if (arr[i].Id === 0) {
                                   arr.splice(i, 1);
                                }
                            }
                            return arr;
                    } else 
                    {
                    // invalid response
                        return $q.reject(response.data);
                    }
                }, function(response) {
                    // something went wrong
                    return $q.reject(response.data);
                });
    }

    factory.getNewEmployee = function() {
        return $http.get(urlNewEmployee)
                .then(function(response) {
                    if (typeof response.data === 'object') {
                            return response.data;
                    } else 
                    {
                    // invalid response
                        return $q.reject(response.data);
                    }
                }, function(response) {
                    // something went wrong
                    return $q.reject(response.data);
                });   
    }

    factory.getAvarcomsUrl =  function() {
        return urlAvarcoms;
    };

    factory.getTaxiDriversUrl = function() {
        return urlTaxiDrivers;
    };

    factory.insertEmployee = function(obj){
        var res;
        return res = $http
        ({
                   url: urlBase + 'employee/',
                   method: 'POST',
                   headers: { 'Content-Type':'application/json; charset=utf-8'},
                   data: JSON.stringify(obj)
        })
        .then(function(response) {
            obj.Id = response.data.Id;
        }, 
        function(response) { // optional
                console.log('POST request has failed: ' + urlBase + 'employee/');
        });
    };

    factory.updateEmployee = function(obj){
        var res;
        return res = $http
        ({
                   url: urlUpdateEmployee,
                   //method: 'PUT',
                   method: 'POST',
                   headers: { 'Content-Type':'application/json; charset=utf-8'},
                   data: JSON.stringify(obj)
        })
        .then(function(response) {
            obj.Id = response.data.Id;
        }, 
        function() { // optional
               console.log('PUT request has failed: ' + urlBase + 'employee/');
        });
    };

    factory.deleteEmployee = function(obj){
        var res;
        return res = $http
        ({
                   url: urlDeleteEmployee,
                   //method: 'DELETE',
                   method: 'POST',
                   headers: { 'Content-Type':'application/json; charset=utf-8'},
                   data: JSON.stringify(obj)
        })
        .then(function() {
                   
        }, 
        function() { // optional
                // failed
        });
    };

    factory.getCoords = function(arr){
        var ids = [];

        arr.forEach(function(item, i, someArr) {
            ids.push(item.Id);
        });

        var res;
        return res = $http
        ({
                   url: urlAvarcomCoords,
                   method: 'POST',
                   headers: { 'Content-Type':'application/json; charset=utf-8'},
                   data: JSON.stringify(ids)
        })
        .then(function(response) {
          if (typeof response.data === 'object') {
                    return response.data;
            } else 
            {
            // invalid response
                return $q.reject(response.data);
            }  
        }, 
        function() { // optional
                console.log('POST request has failed: ' + urlAvarcomCoords);
        });
    };

    return factory;
}]);