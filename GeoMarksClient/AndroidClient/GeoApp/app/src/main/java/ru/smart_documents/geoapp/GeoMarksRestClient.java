package ru.smart_documents.geoapp;

import android.content.Context;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.ResponseHandlerInterface;
import com.loopj.android.http.SyncHttpClient;

import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.client.params.ClientPNames;

/**
 * Created by nail on 17.04.2016.
 */
public class GeoMarksRestClient {
    //private static final String BASE_URL = "http://192.168.0.100:8081/GeoMarksRest/GeoMarksRestSvc.svc/";
    //private static final String BASE_URL = "http://192.168.43.61:8081/GeoMarksRest/GeoMarksRestSvc.svc/";
    //private static final String BASE_URL = "http://192.168.1.52:8081/GeoMarksRest/GeoMarksRestSvc.svc/";
    //private static final String BASE_URL = "http://192.168.1.96:8081/GeoMarksRest/GeoMarksRestSvc.svc/";
    //private static final String BASE_URL = "http://192.168.0.106:49545/GeoMarksRestSvc.svc/";

    // Мини продакшн сервер
    //private static final String BASE_URL = "http://dtp.smart-documents.ru:8081/GeoMarksRest/GeoMarksRestSvc.svc/";

    // Продакшн сервер
    private static final String BASE_URL = "http://195.151.216.17:8081/GeoMarksRest/GeoMarksRestSvc.svc/";

    private static SyncHttpClient client = new SyncHttpClient();

    public static void get(String url, RequestParams params, ResponseHandlerInterface responseHandler) {
        //client.getHttpClient().getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
        client.setEnableRedirects(true);
        client.setTimeout(40000);
        //client.setMaxRetriesAndTimeout(2, 30000);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams  params, ResponseHandlerInterface responseHandler) {
        client.setEnableRedirects(true);
        //client.setMaxRetriesAndTimeout(2, 30000);
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, HttpEntity entity, String contentType, ResponseHandlerInterface responseHandler) {
        client.setEnableRedirects(true);
        client.setTimeout(40000);
        //client.setMaxRetriesAndTimeout(2, 30000);
        client.post(context, getAbsoluteUrl(url), entity, contentType, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}
