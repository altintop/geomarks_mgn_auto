﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksModel.Mappers
{
    public class UserRoleMap : ClassMap<UserRole>
    {

        public UserRoleMap()
        {
            Table("user_role");

            Id(x => x.Id).GeneratedBy.Identity().Column("id");

            Map(x => x.Name).Column("name").Not.Nullable();
            Map(x => x.Alias).Column("alias").Not.Nullable();

            HasMany(x => x.Users).Inverse().Not.LazyLoad();
        }
    }
}
