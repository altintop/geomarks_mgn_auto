﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksModel.Mappers
{
    public class UserCoordinatesMap : ClassMap<UserCoordinates>
    {
        public UserCoordinatesMap()
        {
            Table("user_coordinates");

            Id(x => x.Id).GeneratedBy.Identity().Column("id");

            Map(x => x.Latitude).Column("latitude").Not.Nullable();
            Map(x => x.Longitude).Column("longitude").Not.Nullable();
            Map(x => x.UpdateTime).Column("updateTime").Not.Nullable();

            HasOne(x => x.User).PropertyRef(x => x.CurrentCoordinates).Not.LazyLoad();
        }
    }
}
