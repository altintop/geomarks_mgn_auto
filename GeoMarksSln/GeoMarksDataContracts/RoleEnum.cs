﻿using System.Runtime.Serialization;

namespace SmartDocuments.GeoMarksDataContracts
{
    /// <summary>
    /// Enum отвечающий за роли пользователей
    /// </summary>
    [DataContract]
    public enum RoleEnum
    {
        [EnumMember]
        Commissar = 1,
        [EnumMember]
        Manager = 2,
        [EnumMember]
        Informant = 3,
        [EnumMember]
        Admin = 4
    }
}
