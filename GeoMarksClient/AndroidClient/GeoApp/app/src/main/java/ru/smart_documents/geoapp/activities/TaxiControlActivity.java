package ru.smart_documents.geoapp.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.pm.PackageManager;
/*import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
*/
import android.location.Location;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import ru.smart_documents.geoapp.GeoMarksRestClient;
import ru.smart_documents.geoapp.GeocoderHelper;
import ru.smart_documents.geoapp.MyProperties;
import ru.smart_documents.geoapp.R;
import ru.smart_documents.geoapp.dtos.DefaultCommentInfo;
import ru.smart_documents.geoapp.dtos.UserInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class TaxiControlActivity extends AppCompatActivity implements ConnectionCallbacks,
        OnConnectionFailedListener, LocationListener  {

    // UI views
    private View mProgressView;
    private EditText mCommentEditText;
    private Spinner mCommentSpinner;
    private Button sendCoordsButton = null;

    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 1000; // 1 sec
    private static int FASTEST_INTERVAL = 1000; // 1 sec
    private static int DISPLACEMENT = 2; // 2 meters

    private UserInfo mUserInfo;
    //private LocationManager mLocationManager;
    private SendCoordsToSvcTask mSendCoordsTask = null;
    private GetDefCommentsTask mGetDefComsTask = null;
    private List<DefaultCommentInfo> mDefComments = new ArrayList<DefaultCommentInfo>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taxi_control);
        // ставим ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.taxi_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // вешаем действие на кнопку
        sendCoordsButton = (Button) findViewById(R.id.send_coords_in_button);
        sendCoordsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGeoCoordinates();
            }
        });

        mProgressView = findViewById(R.id.taxi_control_progress);
        mCommentEditText = (EditText)findViewById(R.id.comment_editText);
        mCommentSpinner = (Spinner) findViewById(R.id.comments_spinner);

        // получаем список деф. комментов
        getDefaultComments();

        // We need to check availability of play services
        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // выводим ФИО юзера в шапку
        mUserInfo = MyProperties.getInstance().userInfo;
        getSupportActionBar().setSubtitle(mUserInfo.getFirstName() + " " + mUserInfo.getLastName());
        // Проверяем разрешения на геолокацию
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            checkPlayServices();
            // Resuming the periodic location updates
            if (mGoogleApiClient.isConnected()) {
                startLocationUpdates();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Приложению необходимы разрешения для доступа к данным о местоположении", Toast.LENGTH_LONG).show();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int REQUEST_CODE_ASK_PERMISSIONS = 123;
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
        // Проверяем разрешения на запись файла логов
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                final int REQUEST_CODE_ASK_PERMISSIONS = 124;
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        stopLocationUpdates();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Метод вызывает task для получения списка деф. комментариев
     */
    private void getDefaultComments() {
        if (mGetDefComsTask != null)
            return;
        sendCoordsButton.setEnabled(false);
        //showProgress(true);

        mGetDefComsTask = new GetDefCommentsTask();
        mGetDefComsTask.execute((Void) null);
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    /**
     * Creating location request object
     * */
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    /**
     * Method to verify google play services on the device
     * */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Starting the location updates
     * */
    protected void startLocationUpdates() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    /**
     * Stopping location updates
     */
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private String dateToString(Date date) {
        String DATE_FORMAT_NOW = "dd.MM.yyyy HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(date);
    }

     private void sendGeoCoordinates() {
        //showProgress(true);
        sendCoordsButton.setEnabled(false);
        if ((ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)) {
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                double latitude = mLastLocation.getLatitude();
                double longitude = mLastLocation.getLongitude();
                if (mSendCoordsTask != null)
                    return;
                mSendCoordsTask = new SendCoordsToSvcTask(latitude, longitude, mCommentEditText.getText().toString());
                mSendCoordsTask.execute((Void) null);
            }
            else {
                Toast.makeText(getApplicationContext(), "Данные о местоположении пока не получены. Попробуйте позже", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Приложению необходимы разрешения для доступа к данным о местоположении", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Shows the progress UI
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        String error = "Connection failed: ConnectionResult.getErrorCode() = "  + connectionResult.getErrorCode();
    }

    /**
     * Таск отсылает координаты на сервер через REST-сервис
     */
    public class SendCoordsToSvcTask extends AsyncTask<Void, Void, Boolean> {

        private final double mLatitude;
        private final double mLongitude;
        private final String mComment;
        private String mAddress;

        SendCoordsToSvcTask(double latitude, double longitude, String comment) {
            mLatitude = latitude;
            mLongitude = longitude;
            mComment = comment;
            try {
                mAddress = GeocoderHelper.getAddressLine(TaxiControlActivity.this, latitude, longitude);
            } catch (IOException e) {
                mAddress = "";
                e.printStackTrace();
            }
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            final boolean[] result = {false};
            GeoMarksRestClient client = new GeoMarksRestClient();
            RequestParams rp = new RequestParams();
            rp.put("userId", mUserInfo.getId());
            rp.put("latitude", mLatitude);
            rp.put("longitude", mLongitude);
            rp.put("comment", mComment);
            rp.put("address", mAddress);
            client.get("CreateMark", rp, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        if (response.getString("CreateMarkResult").toLowerCase().equals("true")) {
                            result[0] = true;
                        }
                        else {
                            result[0] = false;
                        }
                    } catch (JSONException exc) {}
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if(statusCode == 503){
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }});
                    }
                    else if(statusCode == 400){
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                            }});
                    }
                    else if(statusCode == 406){
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Дублируемая метка!", Toast.LENGTH_LONG).show();
                            }});
                    }
                    // When Http response code other than 400, 503, 406
                    else{
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }});
                        //Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                    }
                    result[0] = false;
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, final JSONObject errorResponse) {
                    if(statusCode == 503){
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }});
                    }
                    else if(statusCode == 400){
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                            }});
                    }
                    else if(statusCode == 406){
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Toast.makeText(getApplicationContext(), errorResponse.get("DetailedInformation").toString(), Toast.LENGTH_LONG).show();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }});
                    }
                    // When Http response code other than 400, 503, 406
                    else{
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }});
                    }
                    result[0] = false;
                }
            });
            return result[0];
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            //showProgress(false);
            sendCoordsButton.setEnabled(true);
            mSendCoordsTask = null;

            if (success) {
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.good);
                mp.start();
                Toast.makeText(getApplicationContext(), "Координаты отправлены!", Toast.LENGTH_SHORT).show();
            } else {
                MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.negative);
                mp.start();
                Toast.makeText(getApplicationContext(), "Координаты не отправлены!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            //showProgress(false);
            sendCoordsButton.setEnabled(true);
            mSendCoordsTask = null;
        }
    }

    /**
     * Таск получает список дефолтных комментариев через REST-сервис
     */
    public class GetDefCommentsTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            final boolean[] result = {false};
            GeoMarksRestClient client = new GeoMarksRestClient();
            RequestParams rp = new RequestParams();
            client.get("defcomments", rp, new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                    try {
                        if (response.length() > 0) {
                            mDefComments = new ArrayList<DefaultCommentInfo>();
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject dcObject = (JSONObject) response.get(i);
                                DefaultCommentInfo dci = new DefaultCommentInfo();
                                dci.setId(dcObject.getInt("Id"));
                                dci.setText(dcObject.getString("Text"));
                                dci.setAlias(dcObject.getString("Alias"));
                                mDefComments.add(dci);
                            }
                            result[0] = true;
                        }
                    } catch (JSONException exc) {
                        result[0] = false;
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    if (statusCode == 503) {
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else if (statusCode == 400) {
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    // When Http response code other than 400, 503
                    else {
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    result[0] = false;
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    if (statusCode == 503) {
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Сервис недоступен", Toast.LENGTH_LONG).show();
                            }
                        });
                    } else if (statusCode == 400) {
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Ошибка на стороне сервера", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    // When Http response code other than 400, 503
                    else {
                        TaxiControlActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Нет соединения с сервером", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    result[0] = false;
                }
            });
            return result[0];
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mGetDefComsTask = null;
            //showProgress(false);
            sendCoordsButton.setEnabled(true);

            if (success) {
                if (mDefComments.size() > 0) {
                    String[] data = new String[mDefComments.size() + 1];
                    for (int j = 0; j < mDefComments.size(); j++) {
                        data[j] = mDefComments.get(j).getText();
                    }
                    data[mDefComments.size()] = "Другое";
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(TaxiControlActivity.this, R.layout.spinner_item, data);
                    adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
                    mCommentSpinner.setAdapter(adapter);
                    // заголовок
                    mCommentSpinner.setPrompt("Выберите комментарий");
                    // выделяем элемент
                    //commentSpinner.setSelection(2);
                    // устанавливаем обработчик нажатия
                    mCommentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view,
                                                   int position, long id) {
                            // Получаем выбранный объект
                            Object item = parent.getItemAtPosition(position);
                            mCommentEditText.setText(item.toString());
                        }
                        @Override
                        public void onNothingSelected(AdapterView<?> arg0) {
                        }
                    });
                }
            } else {
                Toast.makeText(getApplicationContext(), "Стандартные комментарии не получены!", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected void onCancelled() {
            mGetDefComsTask = null;
            sendCoordsButton.setEnabled(true);
            //showProgress(false);
        }
    }
}
