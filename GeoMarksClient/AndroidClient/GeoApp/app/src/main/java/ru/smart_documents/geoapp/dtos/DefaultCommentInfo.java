package ru.smart_documents.geoapp.dtos;

/**
 * Created by nail on 25.04.2016.
 */
public class DefaultCommentInfo {
    private int id;
    private String text;
    private String alias;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
