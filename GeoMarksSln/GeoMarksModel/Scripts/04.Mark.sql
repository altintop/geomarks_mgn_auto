﻿-- Table: mark

-- DROP TABLE mark;

CREATE TABLE mark
(
  id serial NOT NULL, -- Идентификатор метки
  createtime time with time zone NOT NULL, -- Время отметки
  createuserid integer NOT NULL, -- Пользователь, создавший эту отметку
  assigneduserid integer NOT NULL, -- Пользователь (аварком), которому назначена отметка
  latitude real NOT NULL, -- Координаты метки (широта)
  longitude real NOT NULL, -- Координаты метки (долгота)
  CONSTRAINT pk_mark_id PRIMARY KEY (id),
  CONSTRAINT fk_mark_to_user_assigned FOREIGN KEY (assigneduserid)
      REFERENCES user_gm (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_mark_to_user_creater FOREIGN KEY (createuserid)
      REFERENCES user_gm (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE mark
  OWNER TO postgres;
COMMENT ON TABLE mark
  IS 'Таблица отметок';
COMMENT ON COLUMN mark.id IS 'Идентификатор метки';
COMMENT ON COLUMN mark.createtime IS 'Время отметки';
COMMENT ON COLUMN mark.createuserid IS 'Пользователь, создавший эту отметку';
COMMENT ON COLUMN mark.assigneduserid IS 'Пользователь (аварком), которому назначена отметка';
COMMENT ON COLUMN mark.latitude IS 'Координаты метки (широта)';
COMMENT ON COLUMN mark.longitude IS 'Координаты метки (долгота)';


-- Index: fki_mark_to_user_assigned

-- DROP INDEX fki_mark_to_user_assigned;

CREATE INDEX fki_mark_to_user_assigned
  ON mark
  USING btree
  (assigneduserid);

