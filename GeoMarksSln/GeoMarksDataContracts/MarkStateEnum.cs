﻿using System.Runtime.Serialization;

namespace SmartDocuments.GeoMarksDataContracts
{
    /// <summary>
    /// Enum отвечающий за состояние отметок
    /// </summary>
    [DataContract]
    public enum MarkStateEnum
    {
        [EnumMember]
        New = 1,
        [EnumMember]
        Assigned = 2,
        [EnumMember]
        Accompanied = 3,
        [EnumMember]
        Completed = 4,
        [EnumMember]
        InHand = 5,
        [EnumMember]
        Refused = 6,
        [EnumMember]
        Dispute = 7,
        [EnumMember]
        Hurt = 8,
        [EnumMember]
        Disappeared = 9,
        [EnumMember]
        Mistaken = 10,
        [EnumMember]
        Late = 11
    }
}
