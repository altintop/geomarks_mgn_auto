﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SmartDocuments.GeoMarksDataContracts.DTOs
{
    [DataContract(IsReference = true)]
    public class MarkState
    {
        public MarkState()
        {
            Marks = new List<Mark>();
        }

        [DataMember(Order = 1)]
        public virtual int Id { get; protected set; }
        [DataMember(Order = 2)]
        public virtual string Name { get; set; }
        [DataMember(Order = 3)]
        public virtual string Alias { get; set; }
        [DataMember(Order = 4)]
        public virtual IList<Mark> Marks { get; set; }
    }
}
