﻿using System;
using System.Runtime.Serialization;

namespace SmartDocuments.GeoMarksDataContracts.DTOs
{
    [DataContract(IsReference = true)]
    public class Mark
    {
        [DataMember(Order = 1)]
        public virtual int Id { get; protected set; }
        [DataMember(Order = 2)]
        public virtual double Longitude { get; set; }
        [DataMember(Order = 3)]
        public virtual double Latitude { get; set; }
        [DataMember(Order = 4)]
        public virtual DateTime CreateTime { get; set; }
        [DataMember(Order = 5)]
        public virtual User CreateUser { get; set; }
        [DataMember(Order = 6)]
        public virtual User AssignedUser { get; set; }
        [DataMember(Order = 7)]
        public virtual MarkState State { get; set; }
        [DataMember(Order = 8)]
        public virtual DateTime LastUpdatedTime { get; set; }
        [DataMember(Order = 9)]
        public virtual string Comment { get; set; }
        [DataMember(Order = 10)]
        public virtual string Address { get; set; }
    }
}
