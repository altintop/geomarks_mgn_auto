﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using SmartDocuments.GeoMarksDataContracts;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksSoapService
{
    [ServiceContract]
    public interface IGeoMarksSoapSvc
    {
        [OperationContract]
        string GetData(int value);

        /// <summary>
        /// Метод получения всех меток
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<Mark> GetMarks();

        /// <summary>
        /// Метод получения всех статусов меток
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<MarkState> GetMarkStates();

        /// <summary>
        /// Метод получения всех аваркомов
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<User> GetCommissars();

        /// <summary>
        /// Метод вставки нового юзера
        /// </summary>
        /// <param name="newUser">Частично заполненный объект с данными о пользователе</param>
        /// <param name="userRole">Значение роли</param>
        /// <returns></returns>
        [OperationContract]
        bool InsertNewUser(User newUser, RoleEnum userRole);

        /// <summary>
        /// Метод проверки существования пользователя с указанным email-ом и паролем
        /// </summary>
        /// <param name="email">Почтовый адрес пользователя</param>
        /// <param name="password">Пароль к системе</param>
        /// <returns></returns>
        [OperationContract]
        User ValidateUser(string email, string password);
    }
}
