﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksModel.Mappers
{
    public class UserMap : ClassMap<User>
    {

        public UserMap()
        {
            Table("user_gm");

            Id(x => x.Id).GeneratedBy.Identity().Column("id");

            Map(x => x.PhoneNumber).Column("phonenumber").Not.Nullable();
            Map(x => x.Email).Column("email").Not.Nullable();
            Map(x => x.FirstName).Column("firstname").Not.Nullable();
            Map(x => x.Password).Column("password").Not.Nullable();
            Map(x => x.LastName).Column("lastname").Not.Nullable();
            Map(x => x.Patronymic).Column("patronymic");

            References(x => x.Role).Not.Nullable().Not.LazyLoad();

            References(x => x.CurrentCoordinates).Unique().Not.LazyLoad();

            HasMany(x => x.CreatedMarks).Inverse().Not.LazyLoad();
            HasMany(x => x.AssignedMarks).Inverse().Not.LazyLoad();
        }
}
}
