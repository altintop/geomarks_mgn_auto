﻿using SmartDocuments.GeoMarksModel.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using SmartDocuments.GeoMarksDataContracts;
using SmartDocuments.GeoMarksDataContracts.DTOs;

namespace SmartDocuments.GeoMarksSoapService
{
    public class GeoMarksSoapSvc : IGeoMarksSoapSvc
    {
        public string GetData(int value)
        {
            IRepository<MarkState> repo = new NHibernateRepository<MarkState>();
            string res = repo.GetAll().Count.ToString();
            return res;
        }

        public List<User> GetCommissars()
        {
            var repo = new NHibernateRepository<User>();
            return repo.GetAll().Where(_ => _.Role.Alias == "Commissar").ToList();
        }

        public List<Mark> GetMarks()
        {
            var repo = new NHibernateRepository<Mark>();
            return repo.GetAll().ToList();
        }

        public List<MarkState> GetMarkStates()
        {
            var repo = new NHibernateRepository<MarkState>();
            return repo.GetAll().ToList();
        }

        public bool InsertNewUser(User newUser, RoleEnum userRole)
        {
            var emptyStr = "Незаполнено поле ";
            if (string.IsNullOrEmpty(newUser.Email)) throw new Exception(emptyStr + "email");
            if (string.IsNullOrEmpty(newUser.FirstName)) throw new Exception(emptyStr + "firstName");
            if (string.IsNullOrEmpty(newUser.LastName)) throw new Exception(emptyStr + "lastName");
            if (string.IsNullOrEmpty(newUser.Password)) throw new Exception(emptyStr + "password");
            if (string.IsNullOrEmpty(newUser.PhoneNumber)) throw new Exception(emptyStr + "phoneNumber");

            var urRepo = new NHibernateRepository<UserRole>();
            newUser.Role = urRepo.Get((int)userRole);

            var userRepo = new NHibernateRepository<User>();
            userRepo.Save(newUser);

            return true;
        }

        public User ValidateUser(string email, string password)
        {
            try
            {
                var repo = new NHibernateRepository<User>();
                return repo.GetAll().First(_ => _.Email == email && _.Password == password);
            }
            catch (Exception exception)
            {
                NLog.LogManager.GetCurrentClassLogger().Error(exception, "ValidateUser exception!");
                return null;
            }
        }
    }
}
