INSERT INTO mark_state(
            id, name, alias)
    VALUES (1, '�����', 'New');

INSERT INTO mark_state(
            id, name, alias)
    VALUES (2, '���������', 'Assigned');

INSERT INTO mark_state(
            id, name, alias)
    VALUES (3, '��������������', 'Accompanied');

INSERT INTO mark_state(
            id, name, alias)
    VALUES (4, '���������', 'Completed');

--------------------------------------------------------------------------------

INSERT INTO user_role(
            id, alias, name)
    VALUES (1, 'Commissar', '�������');

INSERT INTO user_role(
            id, alias, name)
    VALUES (2, 'Manager', '��������');

INSERT INTO user_role(
            id, alias, name)
    VALUES (3, 'TaxiDriver', '�������');
INSERT INTO user_role(
            id, alias, name)
    VALUES (4, 'Admin', '�������������');

--------------------------------------------------------------------------------
INSERT INTO user_gm(email, password, firstName, lastName, role_id, phonenumber)
    VALUES (0, '�� ��������', '�� ��������', '�� ��������', '�� ��������', 1, '�� ��������');
	
INSERT INTO user_gm(email, password, firstName, lastName, role_id, phonenumber)
    VALUES ('masha@gmail.com', 'masha', '�����', '��������', 2, '9872345679');
	
INSERT INTO user_gm(email, password, firstName, lastName, role_id, phonenumber)
    VALUES ('sasha@gmail.com', 'shura', '���������', '���������', 3, '9872345670');

INSERT INTO user_gm(email, password, firstName, lastName, role_id, phonenumber)
    VALUES ('jack@gmail.com', 'sparrow', 'Jack', 'Sparrow', 1, '9872345671');
	
INSERT INTO user_gm(email, password, firstName, lastName, role_id, phonenumber, patronymic)
    VALUES ('yesenin@gmail.com', 'serg', '������', '������', 1, '9872345671', '�������������');
	
INSERT INTO user_gm(email, password, firstName, lastName, role_id, phonenumber, patronymic)
    VALUES ('admin@gmail.com', 'admin', 'admin', 'admin', 4, '9872345671', 'admin');

--------------------------------------------------------------------------------

INSERT INTO mark(createtime, createuser_id, latitude, longitude, assigneduser_id, state_id) VALUES (now(), 1, 40.060316, -38.432044, 1, 1);
INSERT INTO mark(createtime, createuser_id, latitude, longitude, assigneduser_id, state_id) VALUES (now(), 2, 10.060316, 48.432044, 1, 1);

INSERT INTO default_comment(text, alias) VALUES ('�� �����������', 'crossroad');

INSERT INTO default_comment(text, alias) VALUES ('�� ����', 'corner');

INSERT INTO default_comment(text, alias) VALUES ('����� ���������', 'nearBusStop');

INSERT INTO default_comment(text, alias) VALUES ('� ������ �����', 'streetHead');
